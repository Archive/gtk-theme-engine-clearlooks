/* Clearlooks - a cairo based GTK+ engine
 * Copyright (C) 2006 Richard Stellingwerff <remenic@gmail.com>
 * Copyright (C) 2006 Daniel Borgman <daniel.borgmann@gmail.com>
 * Copyright (C) 2007 Benjamin Berg <benjamin@sipsolutions.net>
 * Copyright (C) 2007-2008 Andrea Cimitan <andrea.cimitan@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Project contact: <gnome-themes-list@gnome.org>
 *
 */


#include "clearlooks_draw.h"
#include "clearlooks_types.h"

#include "support.h"
#include <ge-support.h>
#include <math.h>

#include <cairo.h>

/* Normal shadings */
#define SHADE_TOP 1.055
#define SHADE_CENTER_TOP 1.01
#define SHADE_CENTER_BOTTOM 0.98
#define SHADE_BOTTOM 0.90

typedef void (*menubar_draw_proto) (cairo_t *cr,
				    GtkThemingEngine *engine,
                                    const MenuBarParameters *menubar,
                                    int x, int y, int width, int height);

static void
clearlooks_draw_inset (cairo_t          *cr,
                       const CairoColor *bg_color,
                       double x, double y, double width, double height,
                       double radius, uint8 corners)
{
	CairoColor shadow;
	CairoColor highlight;
	double line_width;
	double min = MIN (width, height);

	line_width = cairo_get_line_width (cr);

	/* not really sure of shading ratios... we will think */
	ge_shade_color (bg_color, 0.94, &shadow);
	ge_shade_color (bg_color, 1.06, &highlight);

	/* highlight */
	cairo_save (cr);

	cairo_move_to (cr, x, y + height);
	cairo_line_to (cr, x + min / 2.0, y + height - min / 2.0);
	cairo_line_to (cr, x + width - min / 2.0, y + min / 2.0);
	cairo_line_to (cr, x + width, y);
	cairo_line_to (cr, x, y);
	cairo_close_path (cr);
	
	cairo_clip (cr);

	ge_cairo_rounded_rectangle (cr, x + line_width / 2.0, y + line_width / 2.0,
	                            width - line_width, height - line_width,
	                            radius, corners);

	ge_cairo_set_color (cr, &shadow);
	cairo_stroke (cr);
	
	cairo_restore (cr);

	/* shadow */
	cairo_save (cr);

	cairo_move_to (cr, x, y + height);
	cairo_line_to (cr, x + min / 2.0, y + height - min / 2.0);
	cairo_line_to (cr, x + width - min / 2.0, y + min / 2.0);
	cairo_line_to (cr, x + width, y);
	cairo_line_to (cr, x + width, y + height);
	cairo_close_path (cr);
	
	cairo_clip (cr);

	ge_cairo_rounded_rectangle (cr, x + line_width / 2.0, y + line_width / 2.0,
	                            width - line_width, height - line_width,
	                            radius, corners);

	ge_cairo_set_color (cr, &highlight);
	cairo_stroke (cr);

	cairo_restore (cr);
}

static void
clearlooks_draw_shadow (cairo_t *cr, GtkThemingEngine *engine, gfloat radius, int width, int height)
{
	CairoColor shade, shadow;
	cairo_save (cr);

	clearlooks_lookup_shade (engine, 6, (GdkRGBA *) &shade);
	ge_shade_color (&shade, 0.92, &shadow);

	cairo_set_line_width (cr, 1.0);
	cairo_set_line_cap (cr, CAIRO_LINE_CAP_BUTT);

	cairo_set_source_rgba (cr, shadow.r, shadow.g, shadow.b, 0.1);

	cairo_move_to (cr, width - 0.5, radius);
	ge_cairo_rounded_corner (cr, width - 0.5, height - 0.5, radius, CR_CORNER_BOTTOMRIGHT);
	cairo_line_to (cr, radius, height - 0.5);

	cairo_stroke (cr);
	cairo_restore (cr);
}

/* This is copied at least in clearlooks_draw_gummy.c.
 * KEEP IN SYNC IF POSSIBLE! */
static void
clearlooks_draw_top_left_highlight (cairo_t *cr,
				    const CairoColor *color,
				    GtkThemingEngine *engine,
                                    int x, int y, int width, int height)
{
	ClearlooksEngine *clearlooks_engine;
	CairoColor hilight;

	double line_width = cairo_get_line_width (cr);
	double offset = line_width / 2.0;
	double light_top, light_bottom, light_left, light_right;
	gint radius;

	clearlooks_engine = CLEARLOOKS_ENGINE (engine);

	cairo_save (cr);

	cairo_set_line_cap (cr, CAIRO_LINE_CAP_BUTT);

	light_top = y + offset;
	light_bottom = y + height;
	light_left = x + offset;
	light_right = x + width;

#if 0
	if (corners & CR_CORNER_BOTTOMLEFT)
		light_bottom -= radius;
	if (corners & CR_CORNER_TOPRIGHT)
		light_right -= radius;
#endif

	gtk_theming_engine_get (engine,
				gtk_theming_engine_get_state (engine),
				"border-radius", &radius,
				NULL);

	ge_shade_color (color, clearlooks_engine->style_constants->topleft_highlight_shade, &hilight);

	cairo_move_to         (cr, light_left, light_bottom);

	ge_cairo_rounded_corner (cr, light_left, light_top, radius, 0); //corners & CR_CORNER_TOPLEFT);

	cairo_line_to         (cr, light_right, light_top);
	cairo_set_source_rgba (cr, hilight.r, hilight.g, hilight.b,
			       clearlooks_engine->style_constants->topleft_highlight_alpha);
	cairo_stroke          (cr);

	cairo_restore (cr);
}

#ifdef DEVELOPMENT
#warning seems to be very slow in scrollbar_stepper
#endif

static void
clearlooks_draw_highlight_and_shade (cairo_t *cr, const ClearlooksColors *colors,
                                     const ShadowParameters *params,
                                     int width, int height, gdouble radius)
{
	CairoColor hilight;
	CairoColor shadow;
	uint8 corners = params->corners;
	double x = 1.0;
	double y = 1.0;

	ge_shade_color (&colors->bg[0], 1.06, &hilight);
	ge_shade_color (&colors->bg[0], 0.94, &shadow);

	width  -= 2;
	height -= 2;

	cairo_save (cr);

	/* Top/Left highlight */
	if (corners & CR_CORNER_BOTTOMLEFT)
		cairo_move_to (cr, x + 0.5, y+height-radius);
	else
		cairo_move_to (cr, x + 0.5, y+height);

	ge_cairo_rounded_corner (cr, x + 0.5, y + 0.5, radius, corners & CR_CORNER_TOPLEFT);

	if (corners & CR_CORNER_TOPRIGHT)
		cairo_line_to (cr, x+width-radius, y + 0.5);
	else
		cairo_line_to (cr, x+width, y + 0.5);

	if (params->shadow & CL_SHADOW_OUT)
		ge_cairo_set_color (cr, &hilight);
	else
		ge_cairo_set_color (cr, &shadow);

	cairo_stroke (cr);

	/* Bottom/Right highlight -- this includes the corners */
	cairo_arc (cr, x + width - 0.5 - radius, y + radius, radius, G_PI * (3/2.0+1/4.0), G_PI * 2);
	ge_cairo_rounded_corner (cr, x+width - 0.5, y+height - 0.5, radius, corners & CR_CORNER_BOTTOMRIGHT);
	cairo_arc (cr, x + radius, y + height - 0.5 - radius, radius, G_PI * 1/2, G_PI * 3/4);

	if (params->shadow & CL_SHADOW_OUT)
		ge_cairo_set_color (cr, &shadow);
	else
		ge_cairo_set_color (cr, &hilight);

	cairo_stroke (cr);

	cairo_restore (cr);
}

static void
clearlooks_set_border_gradient (cairo_t *cr, const CairoColor *color, double hilight, int width, int height)
{
	cairo_pattern_t *pattern;

	CairoColor bottom_shade;
	ge_shade_color (color, hilight, &bottom_shade);

	pattern	= cairo_pattern_create_linear (0, 0, width, height);
	cairo_pattern_add_color_stop_rgb (pattern, 0, color->r, color->g, color->b);
	cairo_pattern_add_color_stop_rgb (pattern, 1, bottom_shade.r, bottom_shade.g, bottom_shade.b);

	cairo_set_source (cr, pattern);
	cairo_pattern_destroy (pattern);
}

static void
clearlooks_draw_gripdots (cairo_t *cr, GtkThemingEngine *engine, int x, int y,
                          int width, int height, int xr, int yr,
                          float contrast)
{
	CairoColor dark, hilight;
	int i, j;
	int xoff, yoff;
	int x_start, y_start;

	clearlooks_lookup_shade (engine, 4, (GdkRGBA *) &dark);
	ge_shade_color (&dark, 1.5, &hilight);

	/* The "- 1" is because there is no space in front of the first dot. */
	x_start = x + width / 2 - ((xr * 3 - 1) / 2);
	y_start = y + height / 2 - ((yr * 3 - 1) / 2);
	
	for ( i = 0; i < xr; i++ )
	{
		for ( j = 0; j < yr; j++ )
		{
			xoff = 3 * i;
			yoff = 3 * j;

			cairo_rectangle (cr, x_start + xoff, y_start + yoff, 2, 2);
			cairo_set_source_rgba (cr, hilight.r, hilight.g, hilight.b, 0.8+contrast);
			cairo_fill (cr);
			cairo_rectangle (cr, x_start + xoff, y_start + yoff, 1, 1);
			cairo_set_source_rgba (cr, dark.r, dark.g, dark.b, 0.8+contrast);
			cairo_fill (cr);
		}
	}
}

static void
clearlooks_draw_button (cairo_t *cr,
			GtkThemingEngine *engine,
                        int x, int y, int width, int height)
{
	ClearlooksStyleFunctions *style_functions;
	double xoffset = 0, yoffset = 0;
	gint radius;
	GtkStateFlags state;
	CairoColor *fill;
	CairoColor border_normal;
	CairoColor border_disabled;
	CairoColor shadow;

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);

	clearlooks_lookup_shade (engine, 6, (GdkRGBA *) &border_normal);
	clearlooks_lookup_shade (engine, 4, (GdkRGBA *) &border_disabled);

	ge_shade_color (&border_normal, 1.04, &border_normal);
	ge_shade_color (&border_normal, 0.94, &shadow);
	ge_shade_color (&border_disabled, 1.08, &border_disabled);

	state = gtk_theming_engine_get_state (engine);

	gtk_theming_engine_get (engine, state,
				"border-radius", &radius,
				"background-color", &fill,
				NULL);

	cairo_save (cr);

	cairo_translate (cr, x, y);
	cairo_set_line_width (cr, 1.0);

#if 0
	if (params->xthickness >= 3 && params->ythickness >= 3)
	{
		xoffset = 1;
		yoffset = 1;
	}
#endif

	radius = MIN (radius, MIN ((width - 2.0 - xoffset * 2.0) / 2.0, (height - 2.0 - yoffset * 2) / 2.0));

#if 0
	if (params->xthickness >= 3 && params->ythickness >= 3)
	{
		style_functions->draw_inset (cr, &params->parentbg, 0, 0, width, height, radius+1, params->corners);
	}
#endif

	ge_cairo_rounded_rectangle (cr, xoffset+1, yoffset+1,
	                                     width-(xoffset*2)-2,
	                                     height-(yoffset*2)-2,
				    radius, clearlooks_get_corners (engine));

	if ((state & GTK_STATE_FLAG_ACTIVE) == 0)
	{
		cairo_pattern_t *pattern;
		CairoColor top_shade, topmiddle_shade, bottom_shade, middle_shade;

		ge_shade_color (fill, SHADE_TOP, &top_shade);
		ge_shade_color (fill, SHADE_CENTER_TOP, &topmiddle_shade);
		ge_shade_color (fill, SHADE_CENTER_BOTTOM, &middle_shade);
		ge_shade_color (fill, SHADE_BOTTOM, &bottom_shade);

		cairo_save (cr);
		cairo_clip_preserve (cr);

		pattern	= cairo_pattern_create_linear (0, 0, 0, height);
		cairo_pattern_add_color_stop_rgb (pattern, 0.0, top_shade.r, top_shade.g, top_shade.b);
		cairo_pattern_add_color_stop_rgb (pattern, 0.3, topmiddle_shade.r, topmiddle_shade.g, topmiddle_shade.b);
		cairo_pattern_add_color_stop_rgb (pattern, 0.7, middle_shade.r, middle_shade.g, middle_shade.b);
		cairo_pattern_add_color_stop_rgb (pattern, 1.0, bottom_shade.r, bottom_shade.g, bottom_shade.b);
		cairo_set_source (cr, pattern);
		cairo_fill (cr);
		cairo_pattern_destroy (pattern);

		cairo_move_to (cr, width-(xoffset*2)-0.5, 0);
		cairo_line_to (cr, width-(xoffset*2)-0.5, height);
		ge_cairo_set_color (cr, &bottom_shade);
		cairo_stroke (cr);

		/* Draw topleft shadow */
		style_functions->draw_top_left_highlight (cr, fill, engine, xoffset + 1, yoffset + 1,
							  width - 2*(xoffset + 1), height - 2*(yoffset + 1));

		cairo_restore (cr);
	}
	else
	{
		cairo_pattern_t *pattern;

		ge_cairo_set_color (cr, fill);
		cairo_fill_preserve (cr);

		pattern	= cairo_pattern_create_linear (0, 0, 0, height);
		cairo_pattern_add_color_stop_rgba (pattern, 0.0, shadow.r, shadow.g, shadow.b, 0.0);
		cairo_pattern_add_color_stop_rgba (pattern, 0.4, shadow.r, shadow.g, shadow.b, 0.0);
		cairo_pattern_add_color_stop_rgba (pattern, 1.0, shadow.r, shadow.g, shadow.b, 0.2);
		cairo_set_source (cr, pattern);
		cairo_fill_preserve (cr);
		cairo_pattern_destroy (pattern);

		pattern	= cairo_pattern_create_linear (0, yoffset+1, 0, 3+yoffset);
		cairo_pattern_add_color_stop_rgba (pattern, 0.0, shadow.r, shadow.g, shadow.b,
						   (state & GTK_STATE_FLAG_INSENSITIVE) ? 0.125 : 0.32);
		cairo_pattern_add_color_stop_rgba (pattern, 1.0, shadow.r, shadow.g, shadow.b, 0.0);
		cairo_set_source (cr, pattern);
		cairo_fill_preserve (cr);
		cairo_pattern_destroy (pattern);

		pattern	= cairo_pattern_create_linear (xoffset+1, 0, 3+xoffset, 0);
		cairo_pattern_add_color_stop_rgba (pattern, 0.0, shadow.r, shadow.g, shadow.b,
						   (state & GTK_STATE_FLAG_INSENSITIVE) ? 0.125 : 0.32);
		cairo_pattern_add_color_stop_rgba (pattern, 1.0, shadow.r, shadow.g, shadow.b, 0.0);
		cairo_set_source (cr, pattern);
		cairo_fill (cr);
		cairo_pattern_destroy (pattern);
	}

	/* Drawing the border */
	if ((state & GTK_STATE_FLAG_ACTIVE) == 0 &&
	    gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_DEFAULT))
	{
		ge_shade_color (&border_normal, 0.74, &border_normal);
	}

	ge_cairo_inner_rounded_rectangle (cr, xoffset, yoffset, width-(xoffset*2), height-(yoffset*2), radius, clearlooks_get_corners (engine));

	if (state & GTK_STATE_FLAG_INSENSITIVE)
	{
		ge_cairo_set_color (cr, &border_disabled);
	}
	else
	{
		if ((state & GTK_STATE_FLAG_ACTIVE) == 0)
		{
			gboolean is_default;

			is_default = gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_DEFAULT);
			clearlooks_set_border_gradient (cr, &border_normal,
			                                is_default ? 1.1 : 1.3, 0, height);
		}
		else
		{
			ge_shade_color (&border_normal, 1.08, &border_normal);
			ge_cairo_set_color (cr, &border_normal);
		}
	}

	cairo_stroke (cr);

	cairo_restore (cr);

	gdk_rgba_free ((GdkRGBA *) fill);
}

static void
clearlooks_draw_entry (cairo_t *cr,
		       GtkThemingEngine *engine,
                       const FocusParameters  *focus,
                       int x, int y, int width, int height)
{
	CairoColor *base;
	CairoColor border;
	gint radius;
	int xoffset, yoffset;
	GtkStateFlags state;

	state = gtk_theming_engine_get_state (engine);

	gtk_theming_engine_get (engine, state,
				"background-color", &base,
				"border-radius", &radius,
				NULL);

	radius = MIN (radius, MIN ((width - 4.0) / 2.0, (height - 4.0) / 2.0));

	if (state & GTK_STATE_FLAG_INSENSITIVE)
		clearlooks_lookup_shade (engine, 3, (GdkRGBA *) &border);
	else
		clearlooks_lookup_shade (engine, 6, (GdkRGBA *) &border);

	if (state & GTK_STATE_FLAG_FOCUSED)
		border = focus->color;

	cairo_save (cr);

	cairo_translate (cr, x, y);
	cairo_set_line_width (cr, 1.0);

#if 0
	if (params->xthickness >= 3 && params->ythickness >= 3)
	{
		params->style_functions->draw_inset (cr, &params->parentbg, 0, 0, width, height, radius+1, params->corners);
		xoffset = 1;
		yoffset = 1;
	}
	else
#endif
	{
		xoffset = 0;
		yoffset = 0;
	}

	/* Now fill the area we want to be base[NORMAL]. */
	ge_cairo_rounded_rectangle (cr, xoffset + 1, yoffset + 1, width - (xoffset + 1)*2,
	                            height - (yoffset + 1) * 2, MAX(0, radius-1),
				    clearlooks_get_corners (engine));
	ge_cairo_set_color (cr, base);
	cairo_fill (cr);

	/* Draw the inner shadow */
	if (state & GTK_STATE_FLAG_FOCUSED)
	{
		CairoColor focus_shadow;
		ge_shade_color (&border, 1.61, &focus_shadow);
		
		clearlooks_set_mixed_color (cr, base, &focus_shadow, 0.5);
		ge_cairo_inner_rounded_rectangle (cr, xoffset + 1, yoffset + 1,
		                                  width - (xoffset + 1)*2, height - (yoffset + 1)*2,
		                                  MAX(0, radius-1), clearlooks_get_corners (engine));
		cairo_stroke (cr);
	}
	else
	{
		CairoColor shadow;
		ge_shade_color (&border, 0.925, &shadow);

		cairo_set_source_rgba (cr, shadow.r, shadow.g, shadow.b,
				       (state & GTK_STATE_FLAG_INSENSITIVE) ? 0.05 : 0.1);

		cairo_set_line_cap (cr, CAIRO_LINE_CAP_BUTT);
		cairo_move_to (cr, 2.5, height-radius);
		cairo_arc (cr, xoffset + 1.5 + MAX(0, radius-1), yoffset + 1.5 + MAX(0, radius-1),
		           MAX(0, radius-1), G_PI, 270*(G_PI/180));
		cairo_line_to (cr, width-radius, 2.5);
		cairo_stroke (cr);
	}

	ge_cairo_inner_rounded_rectangle (cr, xoffset, yoffset,
	                                  width-2*xoffset, height-2*yoffset,
	                                  radius, clearlooks_get_corners (engine));
	if (state & GTK_STATE_FLAG_FOCUSED ||
	    state & GTK_STATE_FLAG_INSENSITIVE)
		ge_cairo_set_color (cr, &border);
	else
		clearlooks_set_border_gradient (cr, &border, 1.32, 0, height);
	cairo_stroke (cr);

	cairo_restore (cr);

	gdk_rgba_free ((GdkRGBA *) base);
}

static void
clearlooks_draw_entry_progress (cairo_t *cr,
				GtkThemingEngine *engine,
                                const EntryProgressParameters *progress,
                                int x, int y, int width, int height)
{
	CairoColor border;
	CairoColor *fill;
	gint entry_width, entry_height;
	GtkStateFlags state;
	double entry_radius;
	gint radius;

	cairo_save (cr);

	state = gtk_theming_engine_get_state (engine);
	gtk_theming_engine_get (engine, state,
				"background-color", &fill,
				"border-radius", &radius,
				NULL);

	ge_shade_color (fill, 0.9, &border);

	if (progress->max_size_known)
	{
		entry_width = progress->max_size.width + progress->border.left + progress->border.right;
		entry_height = progress->max_size.height + progress->border.top + progress->border.bottom;
		entry_radius = MIN (radius, MIN ((entry_width - 4.0) / 2.0, (entry_height - 4.0) / 2.0));
	}
	else
	{
		entry_radius = radius;
	}

	radius = MAX (0, entry_radius + 1.0 - MAX (MAX (progress->border.left, progress->border.right),
	                                           MAX (progress->border.top, progress->border.bottom)));

	if (progress->max_size_known)
	{
		/* Clip to the max size, and then draw a (larger) rectangle ... */
		ge_cairo_rounded_rectangle (cr, progress->max_size.x,
		                                progress->max_size.y,
		                                progress->max_size.width,
		                                progress->max_size.height,
		                                radius,
		                                CR_CORNER_ALL);
		cairo_clip (cr);

		/* We just draw wider by one pixel ... */
		ge_cairo_set_color (cr, fill);
		cairo_rectangle (cr, x, y + 1, width, height - 2);
		cairo_fill (cr);

		cairo_set_line_width (cr, 1.0);
		ge_cairo_set_color (cr, &border);
		ge_cairo_inner_rectangle (cr, x - 1, y, width + 2, height);
		cairo_stroke (cr);
	}
	else
	{
		ge_cairo_rounded_rectangle (cr, x, y, width + 10, height + 10, radius, CR_CORNER_ALL);
		cairo_clip (cr);
		ge_cairo_rounded_rectangle (cr, x - 10, y - 10, width + 10, height + 10, radius, CR_CORNER_ALL);
		cairo_clip (cr);

		ge_cairo_set_color (cr, fill);
		ge_cairo_rounded_rectangle (cr, x + 1, y + 1, width - 2, height - 2, radius, CR_CORNER_ALL);
		cairo_fill (cr);

		cairo_set_line_width (cr, 1.0);
		ge_cairo_set_color (cr, &border);
		ge_cairo_rounded_rectangle (cr, x + 0.5, y + 0.5, width - 1.0, height - 1.0, radius, CR_CORNER_ALL);
		cairo_stroke (cr);
	}

	cairo_restore (cr);

	gdk_rgba_free ((GdkRGBA *) fill);
}

static void
clearlooks_draw_spinbutton (cairo_t *cr,
			    GtkThemingEngine *engine,
                            int x, int y, int width, int height)
{
	ClearlooksStyleFunctions *style_functions;
	ClearlooksStyleConstants *style_constants;
	CairoColor *bg_color, border;
	CairoColor hilight;
	GtkStateFlags state;
	GtkBorder *button_border;

	state = gtk_theming_engine_get_state (engine);

	if (state & GTK_STATE_FLAG_INSENSITIVE)
		clearlooks_lookup_shade (engine, 5, (GdkRGBA *) &border);
	else
		clearlooks_lookup_shade (engine, 3, (GdkRGBA *) &border);

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, &style_constants);

	gtk_theming_engine_get (engine, state,
				"background-color", &bg_color,
				"border-width", &button_border,
				NULL);

	style_functions->draw_button (cr, engine, x, y, width, height);

	ge_shade_color (bg_color, style_constants->topleft_highlight_shade, &hilight);
	hilight.a = style_constants->topleft_highlight_alpha;

	cairo_translate (cr, x, y);

	cairo_move_to (cr, button_border->left + 0.5,       (height/2) + 0.5);
	cairo_line_to (cr, width - button_border->right - 0.5, (height/2) + 0.5);
	ge_cairo_set_color (cr, &border);
	cairo_stroke (cr);

	cairo_move_to (cr, button_border->left + 0.5,       (height/2)+1.5);
	cairo_line_to (cr, width - button_border->right - 0.5, (height/2)+1.5);
	ge_cairo_set_color (cr, &hilight);
	cairo_stroke (cr);

	gdk_rgba_free ((GdkRGBA *) bg_color);
	gtk_border_free (button_border);
}

static void
clearlooks_draw_spinbutton_down (cairo_t *cr,
				 GtkThemingEngine *engine,
                                 int x, int y, int width, int height)
{
	cairo_pattern_t *pattern;
	gint radius;
	CairoColor *bg_color, shadow;

	gtk_theming_engine_get (engine, 0,
				"border-radius", &radius,
				"background-color", &bg_color,
				NULL);

	ge_shade_color (bg_color, 0.8, &shadow);
	gdk_rgba_free ((GdkRGBA *) bg_color);

	radius = MIN (radius, MIN ((width - 4.0) / 2.0, (height - 4.0) / 2.0));

	gtk_theming_engine_get (engine,
				gtk_theming_engine_get_state (engine),
				"background-color", &bg_color,
				NULL);

	cairo_translate (cr, x+1, y+1);

	ge_cairo_rounded_rectangle (cr, 1, 1, width-4, height-4, radius,
				    clearlooks_get_corners (engine));

	ge_cairo_set_color (cr, bg_color);

	cairo_fill_preserve (cr);

	pattern = cairo_pattern_create_linear (0, 0, 0, height);
	cairo_pattern_add_color_stop_rgb (pattern, 0.0, shadow.r, shadow.g, shadow.b);
	cairo_pattern_add_color_stop_rgba (pattern, 1.0, shadow.r, shadow.g, shadow.b, 0.0);

	cairo_set_source (cr, pattern);
	cairo_fill (cr);

	cairo_pattern_destroy (pattern);

	gdk_rgba_free ((GdkRGBA *) bg_color);
}

static void
clearlooks_scale_draw_gradient (cairo_t *cr,
                                const CairoColor *c1,
                                const CairoColor *c2,
                                const CairoColor *c3,
                                int x, int y, int width, int height,
                                boolean horizontal)
{
	cairo_pattern_t *pattern;

	pattern = cairo_pattern_create_linear (0.5, 0.5, horizontal ? 0.5 :  width + 1, horizontal ? height + 1: 0.5);
	cairo_pattern_add_color_stop_rgb (pattern, 0.0, c1->r, c1->g, c1->b);
	cairo_pattern_add_color_stop_rgb (pattern, 1.0, c2->r, c2->g, c2->b);

	cairo_rectangle (cr, x, y, width, height);
	cairo_set_source (cr, pattern);
	cairo_fill (cr);
	cairo_pattern_destroy (pattern);

	ge_cairo_set_color (cr, c3);
	ge_cairo_inner_rectangle (cr, x, y, width, height);
	cairo_stroke (cr);
}

#define TROUGH_SIZE 7
static void
clearlooks_draw_scale_trough (cairo_t *cr,
			      GtkThemingEngine *engine,
                              const SliderParameters *slider,
                              int x, int y, int width, int height)
{
	ClearlooksStyleFunctions *style_functions;
	int     trough_width, trough_height;
	double  translate_x, translate_y;

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);
	cairo_save (cr);

	if (slider->horizontal)
	{
		trough_width  = width;
		trough_height = TROUGH_SIZE;
		
		translate_x   = x;
		translate_y   = y + (height/2) - (TROUGH_SIZE/2);
	}
	else
	{
		trough_width  = TROUGH_SIZE;
		trough_height = height;
		
		translate_x   = x + (width/2) - (TROUGH_SIZE/2);
		translate_y  = y;
	}

	cairo_set_line_width (cr, 1.0);
	cairo_translate (cr, translate_x, translate_y);

#if 0
	if (!slider->fill_level)
		style_functions->draw_inset (cr, &params->parentbg, 0, 0, trough_width, trough_height, 0, 0);
#endif

	if (!slider->lower && !slider->fill_level)
	{
		CairoColor shade1, shade2, shadow;

		clearlooks_lookup_shade (engine, 2, (GdkRGBA *) &shade1);
		clearlooks_lookup_shade (engine, 4, (GdkRGBA *) &shade2);
		ge_shade_color (&shade1, 0.96, &shadow);

		clearlooks_scale_draw_gradient (cr, &shadow, /* top */
						&shade1, /* bottom */
		                                &shade2, /* border */
		                                1.0, 1.0, trough_width - 2, trough_height - 2,
		                                slider->horizontal);
	}
	else
	{
		CairoColor border, spot1, spot2;

		clearlooks_lookup_spot (engine, 2, (GdkRGBA *) &border);
		clearlooks_lookup_spot (engine, 1, (GdkRGBA *) &spot1);
		clearlooks_lookup_spot (engine, 0, (GdkRGBA *) &spot2);
		border.a = 0.64;

		clearlooks_scale_draw_gradient (cr, &spot1, /* top */
		                                &spot2, /* bottom */
		                                &border, /* border */
		                                1.0, 1.0, trough_width - 2, trough_height - 2,
		                                slider->horizontal);
	}
	cairo_restore (cr);
}

static void
clearlooks_draw_slider (cairo_t *cr,
			GtkThemingEngine *engine,
                        int x, int y, int width, int height)
{
	CairoColor spot, fill, border;
	cairo_pattern_t *pattern;
	GtkStateFlags state;
	gint radius;

	state = gtk_theming_engine_get_state (engine);
	gtk_theming_engine_get (engine, state,
				"border-radius", &radius,
				NULL);

	radius = MIN (radius, MIN ((width - 1.0) / 2.0, (height - 1.0) / 2.0));

	clearlooks_lookup_spot (engine, 1, (GdkRGBA *) &spot);
	clearlooks_lookup_shade (engine, 2, (GdkRGBA *) &fill);

	if (state & GTK_STATE_FLAG_INSENSITIVE)
		clearlooks_lookup_shade (engine, 4, (GdkRGBA *) &border);
	else
		clearlooks_lookup_shade (engine, 6, (GdkRGBA *) &border);

	cairo_set_line_width (cr, 1.0);
	cairo_translate      (cr, x, y);

	if (state & GTK_STATE_FLAG_PRELIGHT)
		clearlooks_lookup_spot (engine, 2, (GdkRGBA *) &border);

	/* fill the widget */
	ge_cairo_rounded_rectangle (cr, 1.0, 1.0, width-2, height-2, radius,
				    clearlooks_get_corners (engine));

	/* Fake light */
	if ((state & GTK_STATE_FLAG_INSENSITIVE) == 0)
	{
		CairoColor top, bot;

		clearlooks_lookup_shade (engine, 0, (GdkRGBA *) &top);
		clearlooks_lookup_shade (engine, 2, (GdkRGBA *) &bot);

		pattern	= cairo_pattern_create_linear (0, 0, 0, height);
		cairo_pattern_add_color_stop_rgb (pattern, 0.0,  top.r, top.g, top.b);
		cairo_pattern_add_color_stop_rgb (pattern, 1.0,  bot.r, bot.g, bot.b);
		cairo_set_source (cr, pattern);
		cairo_fill (cr);
		cairo_pattern_destroy (pattern);
	}
	else
	{
		ge_cairo_set_color (cr, &fill);
		cairo_fill         (cr);
	}

	/* Set the clip */
	cairo_save (cr);
	cairo_rectangle (cr, 1.0, 1.0, 6, height-2);
	cairo_rectangle (cr, width-7.0, 1.0, 6, height-2);
	cairo_clip_preserve (cr);

	cairo_new_path (cr);

	/* Draw the handles */
	ge_cairo_rounded_rectangle (cr, 1.0, 1.0, width-1, height-1, radius,
				    clearlooks_get_corners (engine));
	pattern = cairo_pattern_create_linear (1.0, 1.0, 1.0, 1.0+height);

	if (state & GTK_STATE_FLAG_PRELIGHT)
	{
		CairoColor highlight;
		ge_shade_color (&spot, 1.3, &highlight);
		cairo_pattern_add_color_stop_rgb (pattern, 0.0, highlight.r, highlight.g, highlight.b);
		cairo_pattern_add_color_stop_rgb (pattern, 1.0, spot.r, spot.g, spot.b);
		cairo_set_source (cr, pattern);
	}
	else
	{
		CairoColor hilight;
		ge_shade_color (&fill, 1.3, &hilight);
		cairo_set_source_rgba (cr, hilight.r, hilight.g, hilight.b, 0.5);
	}

	cairo_fill (cr);
	cairo_pattern_destroy (pattern);

	cairo_restore (cr);

	/* Draw the border */
	ge_cairo_inner_rounded_rectangle (cr, 0, 0, width, height, radius,
					  clearlooks_get_corners (engine));

	if (state & GTK_STATE_FLAG_PRELIGHT ||
	    state & GTK_STATE_FLAG_INSENSITIVE)
		ge_cairo_set_color (cr, &border);
	else
		clearlooks_set_border_gradient (cr, &border, 1.2, 0, height);
	cairo_stroke (cr);

	/* Draw handle lines */
	if (width > 14)
	{
		cairo_move_to (cr, 6.5, 1.0);
		cairo_line_to (cr, 6.5, height-1);

		cairo_move_to (cr, width-6.5, 1.0);
		cairo_line_to (cr, width-6.5, height-1);

		cairo_set_line_width (cr, 1.0);
		border.a = (state & GTK_STATE_FLAG_INSENSITIVE) ? 0.6 : 0.3;
		ge_cairo_set_color (cr, &border);
		cairo_stroke (cr);
	}
}

static void
clearlooks_draw_slider_button (cairo_t *cr,
			       GtkThemingEngine *engine,
                               const SliderParameters *slider,
                               int x, int y, int width, int height)
{
	ClearlooksStyleFunctions *style_functions;
	gint radius;

	gtk_theming_engine_get (engine,
				gtk_theming_engine_get_state (engine),
				"border-radius", &radius,
				NULL);

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);

	radius = MIN (radius, MIN ((width - 1.0) / 2.0, (height - 1.0) / 2.0));

	cairo_save (cr);
	cairo_set_line_width (cr, 1.0);

	if (!slider->horizontal)
		ge_cairo_exchange_axis (cr, &x, &y, &width, &height);
	cairo_translate (cr, x, y);

	style_functions->draw_shadow (cr, engine, radius, width, height);
	style_functions->draw_slider (cr, engine, 1, 1, width-2, height-2);

	if (width > 24)
		style_functions->draw_gripdots (cr, engine, 1, 1, width-2, height-2, 3, 3, 0);

	cairo_restore (cr);
}

static void
clearlooks_draw_progressbar_trough (cairo_t *cr,
				    GtkThemingEngine *engine,
                                    int x, int y, int width, int height)
{
	CairoColor        fill, border, shadow;
	cairo_pattern_t  *pattern;
	gint              radius;
	GtkStateFlags     state;

	state = gtk_theming_engine_get_state (engine);
	gtk_theming_engine_get (engine, state,
				"border-radius", &radius,
				NULL);

	clearlooks_lookup_shade (engine, 2, (GdkRGBA *) &fill);
	clearlooks_lookup_shade (engine, 4, (GdkRGBA *) &border);

	radius = MIN (radius, MIN ((height-2.0) / 2.0, (width-2.0) / 2.0));

	cairo_save (cr);

	cairo_set_line_width (cr, 1.0);

	/* Create trough box */
	ge_cairo_rounded_rectangle (cr, x+1, y+1, width-2, height-2, radius,
				    clearlooks_get_corners (engine));
	ge_cairo_set_color (cr, &fill);
	cairo_fill (cr);

	/* Draw border */
	ge_cairo_rounded_rectangle (cr, x+0.5, y+0.5, width-1, height-1, radius,
				    clearlooks_get_corners (engine));
	ge_cairo_set_color (cr, &border);
	cairo_stroke (cr);

	/* clip the corners of the shadows */
	ge_cairo_rounded_rectangle (cr, x+1, y+1, width-2, height-2, radius,
				    clearlooks_get_corners (engine));
	cairo_clip (cr);

	ge_shade_color (&border, 0.925, &shadow);

	/* Top shadow */
	cairo_rectangle (cr, x+1, y+1, width-2, 4);
	pattern = cairo_pattern_create_linear (x, y, x, y+4);
	cairo_pattern_add_color_stop_rgba (pattern, 0.0, shadow.r, shadow.g, shadow.b, 0.2);
	cairo_pattern_add_color_stop_rgba (pattern, 1.0, shadow.r, shadow.g, shadow.b, 0);
	cairo_set_source (cr, pattern);
	cairo_fill (cr);
	cairo_pattern_destroy (pattern);

	/* Left shadow */
	cairo_rectangle (cr, x+1, y+1, 4, height-2);
	pattern = cairo_pattern_create_linear (x, y, x+4, y);
	cairo_pattern_add_color_stop_rgba (pattern, 0.0, shadow.r, shadow.g, shadow.b, 0.2);
	cairo_pattern_add_color_stop_rgba (pattern, 1.0, shadow.r, shadow.g, shadow.b, 0);
	cairo_set_source (cr, pattern);
	cairo_fill (cr);
	cairo_pattern_destroy (pattern);

	cairo_restore (cr);
}

static void
clearlooks_draw_progressbar_fill (cairo_t *cr,
				  GtkThemingEngine *engine,
                                  const ProgressBarParameters *progressbar,
                                  int x, int y, int width, int height,
                                  gint offset)
{
	ClearlooksStyleFunctions *style_functions;
	boolean      is_horizontal = progressbar->orientation < 2;
	double       tile_pos = 0;
	double       stroke_width;
	gint         radius;
	int          x_step;

	cairo_pattern_t *pattern;
	CairoColor       bg_shade;
	CairoColor       border;
	CairoColor       shadow, spot;
	GtkStateFlags    state;

	state = gtk_theming_engine_get_state (engine);
	gtk_theming_engine_get (engine, state,
				"border-radius", &radius,
				NULL);

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);

#if 0
	radius = MAX (0, radius - params->xthickness);
#endif

	cairo_save (cr);

	if (!is_horizontal)
		ge_cairo_exchange_axis (cr, &x, &y, &width, &height);

	if ((progressbar->orientation == CL_ORIENTATION_RIGHT_TO_LEFT) || (progressbar->orientation == CL_ORIENTATION_BOTTOM_TO_TOP))
		ge_cairo_mirror (cr, CR_MIRROR_HORIZONTAL, &x, &y, &width, &height);

	/* Clamp the radius so that the _height_ fits ...  */
	radius = MIN (radius, height / 2.0);

	stroke_width = height;
	x_step = (((float)stroke_width/10)*offset); /* This looks weird ... */

	cairo_translate (cr, x, y);

	cairo_save (cr);
	/* This is kind of nasty ... Clip twice from each side in case the length
	 * of the fill is smaller than twice the radius. */
	ge_cairo_rounded_rectangle (cr, 0, 0, width + radius, height, radius, CR_CORNER_TOPLEFT | CR_CORNER_BOTTOMLEFT);
	cairo_clip (cr);
	ge_cairo_rounded_rectangle (cr, -radius, 0, width + radius, height, radius, CR_CORNER_TOPRIGHT | CR_CORNER_BOTTOMRIGHT);
	cairo_clip (cr);

	/* Draw the background gradient */
	clearlooks_lookup_spot (engine, 1, (GdkRGBA *) &spot);
	ge_shade_color (&spot, 1.1, &bg_shade);

	/* Just leave this disabled, maybe we could use the same gradient
	 * as the buttons in the future, not flat fill */
/*	pattern = cairo_pattern_create_linear (0, 0, 0, height);*/
/*	cairo_pattern_add_color_stop_rgb (pattern, 0.0, bg_shade.r, bg_shade.g, bg_shade.b);*/
/*	cairo_pattern_add_color_stop_rgb (pattern, 0.6, colors->spot[1].r, colors->spot[1].g, colors->spot[1].b);*/
/*	cairo_pattern_add_color_stop_rgb (pattern, 1.0, bg_shade.r, bg_shade.g, bg_shade.b);*/
/*	cairo_set_source (cr, pattern);*/
/*	cairo_paint (cr);*/
/*	cairo_pattern_destroy (pattern);*/

	ge_cairo_set_color (cr, &bg_shade);
	cairo_paint (cr);

	/* Draw the Strokes */
	while (stroke_width > 0 && tile_pos <= width+x_step)
	{
		cairo_move_to (cr, stroke_width/2-x_step, 0);
		cairo_line_to (cr, stroke_width-x_step,   0);
		cairo_line_to (cr, stroke_width/2-x_step, height);
		cairo_line_to (cr, -x_step, height);

		cairo_translate (cr, stroke_width, 0);
		tile_pos += stroke_width;
	}

	clearlooks_lookup_spot (engine, 2, (GdkRGBA *) &spot);
	pattern = cairo_pattern_create_linear (0, 0, 0, height);
	cairo_pattern_add_color_stop_rgba (pattern, 0.0, spot.r, spot.g, spot.b, 0);
	cairo_pattern_add_color_stop_rgba (pattern, 1.0, spot.r, spot.g, spot.b, 0.24);
	cairo_set_source (cr, pattern);
	cairo_fill (cr);
	cairo_pattern_destroy (pattern);

	cairo_restore (cr); /* rounded clip region */

	/* Draw the dark lines and the shadow */
	cairo_save (cr);
	/* Again, this weird clip area. */
	ge_cairo_rounded_rectangle (cr, -1.0, 0, width + radius + 2.0, height, radius, CR_CORNER_TOPLEFT | CR_CORNER_BOTTOMLEFT);
	cairo_clip (cr);
	ge_cairo_rounded_rectangle (cr, -radius - 1.0, 0, width + radius + 2.0, height, radius, CR_CORNER_TOPRIGHT | CR_CORNER_BOTTOMRIGHT);
	cairo_clip (cr);

	shadow.r = 0.0;
	shadow.g = 0.0;
	shadow.b = 0.0;
	shadow.a = 0.1;

	if (progressbar->pulsing)
	{
		/* At the beginning of the bar. */
		cairo_move_to (cr, -0.5 + radius, height + 0.5);
		ge_cairo_rounded_corner (cr, -0.5, height + 0.5, radius + 1, CR_CORNER_BOTTOMLEFT);
		ge_cairo_rounded_corner (cr, -0.5, -0.5, radius + 1, CR_CORNER_TOPLEFT);
		ge_cairo_set_color (cr, &shadow);
		cairo_stroke (cr);
	}
	if (progressbar->value < 1.0 || progressbar->pulsing)
	{
		/* At the end of the bar. */
		cairo_move_to (cr, width + 0.5 - radius, -0.5);
		ge_cairo_rounded_corner (cr, width + 0.5, -0.5, radius + 1, CR_CORNER_TOPRIGHT);
		ge_cairo_rounded_corner (cr, width + 0.5, height + 0.5, radius + 1, CR_CORNER_BOTTOMRIGHT);
		ge_cairo_set_color (cr, &shadow);
		cairo_stroke (cr);
	}

/*	ge_cairo_rounded_rectangle (cr, 1.5,1.5, width-2, height-2, radius, CR_CORNER_ALL);*/
/*	cairo_set_source_rgba (cr, colors->spot[0].r, colors->spot[0].g, colors->spot[0].b, 1);*/
/*	cairo_stroke (cr);*/

	clearlooks_lookup_spot (engine, 1, (GdkRGBA *) &spot);
	style_functions->draw_top_left_highlight (cr, &spot, engine, 1.5, 1.5,
						  width - 1, height - 1);

	clearlooks_lookup_spot (engine, 2, (GdkRGBA *) &border);
	border.a = 0.6;
	ge_cairo_rounded_rectangle (cr, 0.5, 0.5, width-1, height-1, radius, CR_CORNER_ALL);
	ge_cairo_set_color (cr, &border);
	cairo_stroke (cr);

	cairo_restore (cr);

	cairo_restore (cr); /* rotation, mirroring */
}

static void
clearlooks_draw_optionmenu (cairo_t *cr,
			    GtkThemingEngine *engine,
                            const OptionMenuParameters *optionmenu,
                            int x, int y, int width, int height)
{
	ClearlooksStyleFunctions *style_functions;
	SeparatorParameters separator;
	int offset = 0; //params->ythickness + 2;

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);

	style_functions->draw_button (cr, engine, x, y, width, height);

	separator.horizontal = FALSE;
	style_functions->draw_separator (cr, engine, &separator, x+optionmenu->linepos, y + offset, 2, height - offset*2);
}

static void
clearlooks_draw_menu_item_separator (cairo_t                   *cr,
				     GtkThemingEngine          *engine,
                                     const SeparatorParameters *separator,
                                     int x, int y, int width, int height)
{
	CairoColor shade;

	clearlooks_lookup_shade (engine, 5, (GdkRGBA *) &shade);

	cairo_save (cr);
	cairo_set_line_cap (cr, CAIRO_LINE_CAP_BUTT);
	ge_cairo_set_color (cr, &shade);

	if (separator->horizontal)
		cairo_rectangle (cr, x, y, width, 1);
	else
		cairo_rectangle (cr, x, y, 1, height);

	cairo_fill      (cr);

	cairo_restore (cr);
}

static void
clearlooks_draw_menubar0 (cairo_t *cr,
			  GtkThemingEngine *engine,
                          const MenuBarParameters *menubar,
                          int x, int y, int width, int height)
{
	CairoColor dark;

	clearlooks_lookup_shade (engine, 3, (GdkRGBA *) &dark);

	cairo_save (cr);

	cairo_set_line_width (cr, 1);
	cairo_translate (cr, x, y);

	cairo_move_to (cr, 0, height-0.5);
	cairo_line_to (cr, width, height-0.5);
	ge_cairo_set_color (cr, &dark);
	cairo_stroke (cr);

	cairo_restore (cr);
}

static void
clearlooks_draw_menubar2 (cairo_t *cr,
			  GtkThemingEngine *engine,
                          const MenuBarParameters *menubar,
                          int x, int y, int width, int height)
{
	CairoColor *bg_color, lower, shade;
	cairo_pattern_t *pattern;

	cairo_save (cr);

	gtk_theming_engine_get (engine, 0,
				"background-color", &bg_color,
				NULL);

	ge_shade_color (bg_color, 0.96, &lower);

	cairo_translate (cr, x, y);
	cairo_rectangle (cr, 0, 0, width, height);

	/* Draw the gradient */
	pattern = cairo_pattern_create_linear (0, 0, 0, height);
	cairo_pattern_add_color_stop_rgb (pattern, 0.0, bg_color->r,
	                                                bg_color->g,
	                                                bg_color->b);
	cairo_pattern_add_color_stop_rgb (pattern, 1.0, lower.r,
	                                                lower.g,
	                                                lower.b);
	cairo_set_source      (cr, pattern);
	cairo_fill            (cr);
	cairo_pattern_destroy (pattern);

	/* Draw bottom line */
	cairo_set_line_width (cr, 1.0);
	cairo_move_to        (cr, 0, height-0.5);
	cairo_line_to        (cr, width, height-0.5);

	clearlooks_lookup_shade (engine, 3, (GdkRGBA *) &shade);
	ge_cairo_set_color   (cr, &shade);
	cairo_stroke         (cr);

	cairo_restore (cr);

	gdk_rgba_free ((GdkRGBA *) bg_color);
}

static void
clearlooks_draw_menubar1 (cairo_t *cr,
			  GtkThemingEngine *engine,
                          const MenuBarParameters *menubar,
                          int x, int y, int width, int height)
{
	CairoColor border;

	clearlooks_draw_menubar2 (cr, engine, menubar,
	                          x, y, width, height);

	clearlooks_lookup_shade (engine, 3, (GdkRGBA *) &border);
	ge_cairo_set_color (cr, &border);
	ge_cairo_stroke_rectangle (cr, 0.5, 0.5, width-1, height-1);
}


static menubar_draw_proto clearlooks_menubar_draw[3] =
{
	clearlooks_draw_menubar0,
	clearlooks_draw_menubar1,
	clearlooks_draw_menubar2
};

static void
clearlooks_draw_menubar (cairo_t *cr,
			 GtkThemingEngine *engine,
                         const MenuBarParameters *menubar,
                         int x, int y, int width, int height)
{
	if (menubar->style < 0 || menubar->style >= G_N_ELEMENTS (clearlooks_menubar_draw))
		return;

	clearlooks_menubar_draw[menubar->style](cr, engine, menubar,
	                             x, y, width, height);
}

static void
clearlooks_get_frame_gap_clip (int x, int y, int width, int height,
                               const FrameParameters     *frame,
                               ClearlooksRectangle *bevel,
                               ClearlooksRectangle *border)
{
	if (frame->gap_side == CL_GAP_TOP)
	{
		CLEARLOOKS_RECTANGLE_SET (*bevel,  2.0 + frame->gap_x,  0.0,
		                          frame->gap_width - 3, 2.0);
		CLEARLOOKS_RECTANGLE_SET (*border, 1.0 + frame->gap_x,  0.0,
		                         frame->gap_width - 2, 2.0);
	}
	else if (frame->gap_side == CL_GAP_BOTTOM)
	{
		CLEARLOOKS_RECTANGLE_SET (*bevel,  2.0 + frame->gap_x,  height - 2.0,
		                          frame->gap_width - 3, 2.0);
		CLEARLOOKS_RECTANGLE_SET (*border, 1.0 + frame->gap_x,  height - 1.0,
		                          frame->gap_width - 2, 2.0);
	}
	else if (frame->gap_side == CL_GAP_LEFT)
	{
		CLEARLOOKS_RECTANGLE_SET (*bevel,  0.0, 2.0 + frame->gap_x,
		                          2.0, frame->gap_width - 3);
		CLEARLOOKS_RECTANGLE_SET (*border, 0.0, 1.0 + frame->gap_x,
		                          1.0, frame->gap_width - 2);
	}
	else if (frame->gap_side == CL_GAP_RIGHT)
	{
		CLEARLOOKS_RECTANGLE_SET (*bevel,  width - 2.0, 2.0 + frame->gap_x,
		                          2.0, frame->gap_width - 3);
		CLEARLOOKS_RECTANGLE_SET (*border, width - 1.0, 1.0 + frame->gap_x,
		                          1.0, frame->gap_width - 2);
	}
}

static void
clearlooks_draw_frame            (cairo_t *cr,
				  GtkThemingEngine           *engine,
                                  const FrameParameters      *frame,
                                  int x, int y, int width, int height)
{
	CairoColor *bg_color, dark;
	const CairoColor *border = &frame->border;
	ClearlooksRectangle bevel_clip = {0, 0, 0, 0};
	ClearlooksRectangle frame_clip = {0, 0, 0, 0};
	gint radius;
	CairoColor hilight;

	gtk_theming_engine_get (engine, 0,
				"border-radius", &radius,
				"background-color", &bg_color,
				NULL);

	radius = MIN (radius, MIN ((width - 2.0) / 2.0, (height - 2.0) / 2.0));
	clearlooks_lookup_shade (engine, 4, (GdkRGBA *) &dark);

	ge_shade_color (bg_color, 1.05, &hilight);
	gdk_rgba_free ((GdkRGBA *) bg_color);

	if (frame->gap_x != -1)
		clearlooks_get_frame_gap_clip (x, y, width, height,
		                               frame, &bevel_clip, &frame_clip);

	cairo_set_line_width (cr, 1.0);
	cairo_translate      (cr, x, y);

	/* save everything */
	cairo_save (cr);
	/* Set clip for the bevel */
	if (frame->gap_x != -1)
	{
		/* Set clip for gap */
		cairo_set_fill_rule  (cr, CAIRO_FILL_RULE_EVEN_ODD);
		cairo_rectangle      (cr, 0, 0, width, height);
		cairo_rectangle      (cr, bevel_clip.x, bevel_clip.y, bevel_clip.width, bevel_clip.height);
		cairo_clip           (cr);
	}

	/* Draw the bevel */
	if (frame->shadow == CL_SHADOW_ETCHED_IN || frame->shadow == CL_SHADOW_ETCHED_OUT)
	{
		ge_cairo_set_color (cr, &hilight);
		if (frame->shadow == CL_SHADOW_ETCHED_IN)
			ge_cairo_inner_rounded_rectangle (cr, 1, 1, width-1, height-1, radius,
							  clearlooks_get_corners (engine));
		else
			ge_cairo_inner_rounded_rectangle (cr, 0, 0, width-1, height-1, radius,
							  clearlooks_get_corners (engine));
		cairo_stroke (cr);
	}
#if 0
	else if (frame->shadow != CL_SHADOW_NONE)
	{
		ShadowParameters shadow;
		shadow.corners = clearlooks_get_corners (engine);
		shadow.shadow  = frame->shadow;
		clearlooks_draw_highlight_and_shade (cr, engine, &shadow, width, height, radius);
	}
#endif

	/* restore the previous clip region */
	cairo_restore    (cr);
	cairo_save       (cr);
	if (frame->gap_x != -1)
	{
		/* Set clip for gap */
		cairo_set_fill_rule  (cr, CAIRO_FILL_RULE_EVEN_ODD);
		cairo_rectangle      (cr, 0, 0, width, height);
		cairo_rectangle      (cr, frame_clip.x, frame_clip.y, frame_clip.width, frame_clip.height);
		cairo_clip           (cr);
	}

	/* Draw frame */
	if (frame->shadow == CL_SHADOW_ETCHED_IN || frame->shadow == CL_SHADOW_ETCHED_OUT)
	{
		ge_cairo_set_color (cr, &dark);
		if (frame->shadow == CL_SHADOW_ETCHED_IN)
			ge_cairo_inner_rounded_rectangle (cr, 0, 0, width-1, height-1, radius,
							  clearlooks_get_corners (engine));
		else
			ge_cairo_inner_rounded_rectangle (cr, 1, 1, width-1, height-1, radius,
							  clearlooks_get_corners (engine));
	}
	else
	{
		ge_cairo_set_color (cr, border);
		ge_cairo_inner_rounded_rectangle (cr, 0, 0, width, height, radius,
						  clearlooks_get_corners (engine));
	}
	cairo_stroke (cr);

	cairo_restore (cr);
}

static void
clearlooks_draw_tab (cairo_t *cr,
		     GtkThemingEngine       *engine,
                     const TabParameters    *tab,
                     int x, int y, int width, int height)
{
	CairoColor border1, border2, stripe_fill, stripe_border;
	CairoColor       *fill;
	CairoColor        hilight;
	GtkStateFlags     state;

	cairo_pattern_t  *pattern;

	gint              radius;
	double            stripe_size = 2.0;
	double            stripe_fill_size;
	double            length;

	state = gtk_theming_engine_get_state (engine);
	gtk_theming_engine_get (engine, state,
				"border-radius", &radius,
				"background-color", &fill,
				NULL);

	clearlooks_lookup_shade (engine, 6, (GdkRGBA *) &border1);
	clearlooks_lookup_shade (engine, 5, (GdkRGBA *) &border2);
	clearlooks_lookup_spot (engine, 1, (GdkRGBA *) &stripe_fill);
	clearlooks_lookup_spot (engine, 2, (GdkRGBA *) &stripe_border);

	radius = MIN (radius, MIN ((width - 2.0) / 2.0, (height - 2.0) / 2.0));

	cairo_save (cr);

	/* Set clip */
	cairo_rectangle      (cr, x, y, width, height);
	cairo_clip           (cr);
	cairo_new_path       (cr);

	/* Translate and set line width */
	cairo_set_line_width (cr, 1.0);
	cairo_translate      (cr, x, y);


	/* Make the tabs slightly bigger than they should be, to create a gap */
	/* And calculate the strip size too, while you're at it */
	if (tab->gap_side == CL_GAP_TOP || tab->gap_side == CL_GAP_BOTTOM)
	{
		height += 2.0;
		length = height;
		stripe_fill_size = (tab->gap_side == CL_GAP_TOP ? stripe_size/height : stripe_size/(height-2));

		if (tab->gap_side == CL_GAP_TOP)
			cairo_translate (cr, 0.0, -3.0); /* gap at the other side */
	}
	else
	{
		width += 3.0;
		length = width;
		stripe_fill_size = (tab->gap_side == CL_GAP_LEFT ? stripe_size/width : stripe_size/(width-2));

		if (tab->gap_side == CL_GAP_LEFT)
			cairo_translate (cr, -3.0, 0.0); /* gap at the other side */
	}

	/* Set tab shape */
	ge_cairo_rounded_rectangle (cr, 0.5, 0.5, width-1, height-1,
	                            radius, clearlooks_get_corners (engine));

	/* Draw fill */
	ge_cairo_set_color (cr, fill);
	cairo_fill   (cr);


	ge_shade_color (fill, 1.3, &hilight);

	/* Draw highlight */
	if ((state & GTK_STATE_FLAG_ACTIVE) == 0)
	{
#if 0
		ShadowParameters shadow;

		shadow.shadow  = CL_SHADOW_OUT;
		shadow.corners = clearlooks_get_corners (engine);

		clearlooks_draw_highlight_and_shade (cr, colors, &shadow,
		                                     width,
		                                     height, radius);
#endif
	}

	if (state & GTK_STATE_FLAG_ACTIVE)
	{
		CairoColor shadow;
		switch (tab->gap_side)
		{
			case CL_GAP_TOP:
				pattern = cairo_pattern_create_linear (0.5, height-1.5, 0.5, 0.5);
				break;
			case CL_GAP_BOTTOM:
				pattern = cairo_pattern_create_linear (0.5, 1.5, 0.5, height+0.5);
				break;
			case CL_GAP_LEFT:
				pattern = cairo_pattern_create_linear (width-1.5, 0.5, 1.5, 0.5);
				break;
			case CL_GAP_RIGHT:
				pattern = cairo_pattern_create_linear (1.5, 0.5, width-1.5, 0.5);
				break;
			default:
				pattern = NULL;
		}

		ge_cairo_rounded_rectangle (cr, 0.5, 0.5, width-1, height-1, radius, clearlooks_get_corners (engine));

		ge_shade_color (fill, 0.92, &shadow);

		cairo_pattern_add_color_stop_rgba  (pattern, 0.0,        hilight.r, hilight.g, hilight.b, 0.4);
		cairo_pattern_add_color_stop_rgba  (pattern, 1.0/length, hilight.r, hilight.g, hilight.b, 0.4);
		cairo_pattern_add_color_stop_rgb   (pattern, 1.0/length, fill->r,fill->g,fill->b);
		cairo_pattern_add_color_stop_rgb   (pattern, 1.0,        shadow.r,shadow.g,shadow.b);
		cairo_set_source (cr, pattern);
		cairo_fill (cr);
		cairo_pattern_destroy (pattern);
	}
	else
	{
		/* Draw shade */
		switch (tab->gap_side)
		{
			case CL_GAP_TOP:
				pattern = cairo_pattern_create_linear (0.5, height-1.5, 0.5, 0.5);
				break;
			case CL_GAP_BOTTOM:
				pattern = cairo_pattern_create_linear (0.5, 0.5, 0.5, height+0.5);
				break;
			case CL_GAP_LEFT:
				pattern = cairo_pattern_create_linear (width-1.5, 0.5, 0.5, 0.5);
				break;
			case CL_GAP_RIGHT:
				pattern = cairo_pattern_create_linear (0.5, 0.5, width+0.5, 0.5);
				break;
			default:
				pattern = NULL;
		}

		ge_cairo_rounded_rectangle (cr, 0.5, 0.5, width-1, height-1, radius, clearlooks_get_corners (engine));

		cairo_pattern_add_color_stop_rgb  (pattern, 0.0,        stripe_fill.r, stripe_fill.g, stripe_fill.b);
		cairo_pattern_add_color_stop_rgb  (pattern, stripe_fill_size, stripe_fill.r, stripe_fill.g, stripe_fill.b);
		cairo_pattern_add_color_stop_rgba (pattern, stripe_fill_size, hilight.r, hilight.g, hilight.b, 0.5);
		cairo_pattern_add_color_stop_rgba (pattern, 0.8,        hilight.r, hilight.g, hilight.b, 0.0);
		cairo_set_source (cr, pattern);
		cairo_fill (cr);
		cairo_pattern_destroy (pattern);
	}

	ge_cairo_inner_rounded_rectangle (cr, 0, 0, width, height, radius, clearlooks_get_corners (engine));

	if (state & GTK_STATE_FLAG_ACTIVE)
	{
		ge_cairo_set_color (cr, &border2);
		cairo_stroke (cr);
	}
	else
	{
		switch (tab->gap_side)
		{
			case CL_GAP_TOP:
				pattern = cairo_pattern_create_linear (2.5, height-1.5, 2.5, 2.5);
				break;
			case CL_GAP_BOTTOM:
				pattern = cairo_pattern_create_linear (2.5, 2.5, 2.5, height+0.5);
				break;
			case CL_GAP_LEFT:
				pattern = cairo_pattern_create_linear (width-1.5, 2.5, 2.5, 2.5);
				break;
			case CL_GAP_RIGHT:
				pattern = cairo_pattern_create_linear (2.5, 2.5, width+0.5, 2.5);
				break;
			default:
				pattern = NULL;
		}

		cairo_pattern_add_color_stop_rgb (pattern, 0.0,        stripe_border.r, stripe_border.g, stripe_border.b);
		cairo_pattern_add_color_stop_rgb (pattern, stripe_fill_size, stripe_border.r, stripe_border.g, stripe_border.b);
		cairo_pattern_add_color_stop_rgb (pattern, stripe_fill_size, border1.r,       border1.g,       border1.b);
		cairo_pattern_add_color_stop_rgb (pattern, 1.0,        border2.r,       border2.g,       border2.b);
		cairo_set_source (cr, pattern);
		cairo_stroke (cr);
		cairo_pattern_destroy (pattern);
	}

	cairo_restore (cr);

	gdk_rgba_free ((GdkRGBA *) fill);
}

static void
clearlooks_draw_separator (cairo_t *cr,
			   GtkThemingEngine           *engine,
                           const SeparatorParameters  *separator,
                           int x, int y, int width, int height)
{
	CairoColor *bg_color, color, hilight;

	gtk_theming_engine_get (engine, 0,
				"background-color", &bg_color,
				NULL);

	clearlooks_lookup_shade (engine, 2, (GdkRGBA *) &color);
	ge_shade_color (bg_color, 1.065, &hilight);

	cairo_save (cr);
	cairo_set_line_cap (cr, CAIRO_LINE_CAP_BUTT);

	if (separator->horizontal)
	{
		cairo_set_line_width  (cr, 1.0);
		cairo_translate       (cr, x, y+0.5);

		cairo_move_to         (cr, 0.0,   0.0);
		cairo_line_to         (cr, width, 0.0);
		ge_cairo_set_color    (cr, &color);
		cairo_stroke          (cr);

		cairo_move_to         (cr, 0.0,   1.0);
		cairo_line_to         (cr, width, 1.0);
		ge_cairo_set_color    (cr, &hilight);
		cairo_stroke          (cr);
	}
	else
	{
		cairo_set_line_width  (cr, 1.0);
		cairo_translate       (cr, x+0.5, y);

		cairo_move_to         (cr, 0.0, 0.0);
		cairo_line_to         (cr, 0.0, height);
		ge_cairo_set_color    (cr, &color);
		cairo_stroke          (cr);

		cairo_move_to         (cr, 1.0, 0.0);
		cairo_line_to         (cr, 1.0, height);
		ge_cairo_set_color    (cr, &hilight);
		cairo_stroke          (cr);
	}

	cairo_restore (cr);
}

static void
clearlooks_draw_list_view_header (cairo_t *cr,
				  GtkThemingEngine                *engine,
                                  const ListViewHeaderParameters  *header,
                                  int x, int y, int width, int height)
{
	ClearlooksStyleFunctions *style_functions;
	ClearlooksStyleConstants *style_constants;
	CairoColor border, *bg_color;
	GtkStateFlags state;
	CairoColor hilight;
	gboolean is_ltr;

	state = gtk_theming_engine_get_state (engine);
	gtk_theming_engine_get (engine, state,
				"background-color", &bg_color,
				NULL);

	clearlooks_lookup_shade (engine, 4, (GdkRGBA *) &border);
	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, &style_constants);

	ge_shade_color (bg_color,
	                style_constants->topleft_highlight_shade, &hilight);
	hilight.a = style_constants->topleft_highlight_alpha;

	cairo_translate (cr, x, y);
	cairo_set_line_width (cr, 1.0);

	/* Draw highlight */
	if (header->order & CL_ORDER_FIRST)
	{
		cairo_move_to (cr, 0.5, height-1);
		cairo_line_to (cr, 0.5, 0.5);
	}
	else
		cairo_move_to (cr, 0.0, 0.5);

	cairo_line_to (cr, width, 0.5);

	ge_cairo_set_color (cr, &hilight);
	cairo_stroke (cr);

	/* Draw bottom border */
	cairo_move_to (cr, 0.0, height-0.5);
	cairo_line_to (cr, width, height-0.5);
	ge_cairo_set_color (cr, &border);
	cairo_stroke (cr);

	is_ltr = (gtk_theming_engine_get_direction (engine) == GTK_TEXT_DIR_LTR);

	/* Draw resize grip */
	if ((is_ltr && !(header->order & CL_ORDER_LAST)) ||
	    (!is_ltr && !(header->order & CL_ORDER_FIRST)) || header->resizable)
	{
		SeparatorParameters separator;
		separator.horizontal = FALSE;

		if (is_ltr)
			style_functions->draw_separator (cr, engine, &separator,
							 width-1.5, 4.0, 2, height-8.0);
		else
			style_functions->draw_separator (cr, engine, &separator,
							 1.5, 4.0, 2, height-8.0);
	}

	gdk_rgba_free ((GdkRGBA *) bg_color);
}

/* We can't draw transparent things here, since it will be called on the same
 * surface multiple times, when placed on a handlebox_bin or dockitem_bin */
static void
clearlooks_draw_toolbar (cairo_t *cr,
			 GtkThemingEngine                *engine,
                         const ToolbarParameters         *toolbar,
                         int x, int y, int width, int height)
{
	CairoColor *fill;
	CairoColor dark, light;

	gtk_theming_engine_get (engine, 0,
				"background-color", &fill,
				NULL);
	clearlooks_lookup_shade (engine, 3, (GdkRGBA *) &dark);
	ge_shade_color (fill, 1.065, &light);

	cairo_set_line_width (cr, 1.0);
	cairo_translate (cr, x, y);

	ge_cairo_set_color (cr, fill);
	cairo_paint (cr);

	if (!toolbar->topmost)
	{
		/* Draw highlight */
		cairo_move_to       (cr, 0, 0.5);
		cairo_line_to       (cr, width-1, 0.5);
		ge_cairo_set_color  (cr, &light);
		cairo_stroke        (cr);
	}

	/* Draw shadow */
	cairo_move_to       (cr, 0, height-0.5);
	cairo_line_to       (cr, width-1, height-0.5);
	ge_cairo_set_color  (cr, &dark);
	cairo_stroke        (cr);

	gdk_rgba_free ((GdkRGBA *) fill);
}

static void
clearlooks_draw_menuitem (cairo_t *cr,
			  GtkThemingEngine *engine,
                          int x, int y, int width, int height)
{
	CairoColor fill, fill_shade, border;
	cairo_pattern_t *pattern;
	gint radius;

	clearlooks_lookup_spot (engine, 1, (GdkRGBA *) &fill);
	clearlooks_lookup_spot (engine, 2, (GdkRGBA *) &border);

	gtk_theming_engine_get (engine, 0,
				"border-radius", &radius,
				NULL);

	ge_shade_color (&border, 1.05, &border);
	ge_shade_color (&fill, 0.85, &fill_shade);
	cairo_set_line_width (cr, 1.0);

	ge_cairo_rounded_rectangle (cr, x+0.5, y+0.5, width - 1, height - 1, radius, clearlooks_get_corners (engine));

	pattern = cairo_pattern_create_linear (x, y, x, y + height);
	cairo_pattern_add_color_stop_rgb (pattern, 0,   fill.r, fill.g, fill.b);
	cairo_pattern_add_color_stop_rgb (pattern, 1.0, fill_shade.r, fill_shade.g, fill_shade.b);

	cairo_set_source (cr, pattern);
	cairo_fill_preserve  (cr);
	cairo_pattern_destroy (pattern);

	ge_cairo_set_color (cr, &border);
	cairo_stroke (cr);
}

static void
clearlooks_draw_menubaritem (cairo_t *cr,
			     GtkThemingEngine *engine,
			     int x, int y, int width, int height)
{
	CairoColor fill, fill_shade, border;
	cairo_pattern_t *pattern;
	gint radius;

	clearlooks_lookup_spot (engine, 1, (GdkRGBA *) &fill);
	clearlooks_lookup_spot (engine, 2, (GdkRGBA *) &border);

	gtk_theming_engine_get (engine, 0,
				"border-radius", &radius,
				NULL);

	ge_shade_color (&border, 1.05, &border);
	ge_shade_color (&fill, 0.85, &fill_shade);

	cairo_set_line_width (cr, 1.0);
	ge_cairo_rounded_rectangle (cr, x + 0.5, y + 0.5, width - 1, height, radius, clearlooks_get_corners (engine));

	pattern = cairo_pattern_create_linear (x, y, x, y + height);
	cairo_pattern_add_color_stop_rgb (pattern, 0,   fill.r, fill.g, fill.b);
	cairo_pattern_add_color_stop_rgb (pattern, 1.0, fill_shade.r, fill_shade.g, fill_shade.b);

	cairo_set_source (cr, pattern);
	cairo_fill_preserve  (cr);
	cairo_pattern_destroy (pattern);

	ge_cairo_set_color (cr, &border);
	cairo_stroke_preserve (cr);
}

static void
clearlooks_draw_selected_cell (cairo_t                  *cr,
			       GtkThemingEngine         *engine,
	                       int x, int y, int width, int height)
{
	CairoColor *upper_color;
	CairoColor lower_color;
	cairo_pattern_t *pattern;
	GtkStateFlags state;
	cairo_save (cr);

	cairo_translate (cr, x, y);

	state = gtk_theming_engine_get_state (engine);

	if (state & GTK_STATE_FLAG_FOCUSED)
		gtk_theming_engine_get (engine, state, "background-color", &upper_color, NULL);
	else
		gtk_theming_engine_get (engine, GTK_STATE_FLAG_ACTIVE, "background-color", &upper_color, NULL);

	ge_shade_color(upper_color, 0.92, &lower_color);

	pattern = cairo_pattern_create_linear (0, 0, 0, height);
	cairo_pattern_add_color_stop_rgb (pattern, 0.0, upper_color->r,
	                                                upper_color->g,
	                                                upper_color->b);
	cairo_pattern_add_color_stop_rgb (pattern, 1.0, lower_color.r,
	                                                lower_color.g,
	                                                lower_color.b);

	cairo_set_source (cr, pattern);
	cairo_rectangle  (cr, 0, 0, width, height);
	cairo_fill       (cr);

	cairo_pattern_destroy (pattern);

	cairo_restore (cr);

	gdk_rgba_free ((GdkRGBA *) upper_color);
}


static void
clearlooks_draw_scrollbar_trough (cairo_t *cr,
				  GtkThemingEngine *engine,
                                  const ScrollBarParameters        *scrollbar,
                                  int x, int y, int width, int height)
{
	CairoColor        bg, bg_shade, border;
	cairo_pattern_t *pattern;
	GtkStateFlags state;
	gint radius;

	state = gtk_theming_engine_get_state (engine);
	gtk_theming_engine_get (engine, state,
				"border-radius", &radius,
				NULL);

	clearlooks_lookup_shade (engine, 2, (GdkRGBA *) &bg);
	clearlooks_lookup_shade (engine, 5, (GdkRGBA *) &border);

	ge_shade_color (&bg, 0.95, &bg_shade);

	radius = MIN (radius, MIN ((width - 2.0) / 2.0, (height - 2.0) / 2.0));

	cairo_set_line_width (cr, 1);
	/* cairo_translate (cr, x, y); */

	if (scrollbar->horizontal)
		ge_cairo_exchange_axis (cr, &x, &y, &width, &height);

	cairo_translate (cr, x, y);

	/* Draw fill */
	if (radius > 3.0)
		ge_cairo_rounded_rectangle (cr, 1, 0, width-2, height,
		                            radius, clearlooks_get_corners (engine));
	else
		cairo_rectangle (cr, 1, 0, width-2, height);
	ge_cairo_set_color (cr, &bg);
	cairo_fill (cr);

	/* Draw shadow */
	pattern = cairo_pattern_create_linear (1, 0, 3, 0);
	cairo_pattern_add_color_stop_rgb (pattern, 0,   bg_shade.r, bg_shade.g, bg_shade.b);
	cairo_pattern_add_color_stop_rgb (pattern, 1.0, bg.r,      bg.g,      bg.b);
	cairo_rectangle (cr, 1, 0, 4, height);
	cairo_set_source (cr, pattern);
	cairo_fill (cr);
	cairo_pattern_destroy (pattern);

	/* Draw border */
	if (radius > 3.0)
		ge_cairo_rounded_rectangle (cr, 0.5, 0.5, width-1, height-1,
		                            radius, clearlooks_get_corners (engine));
	else
		cairo_rectangle (cr, 0.5, 0.5, width-1, height-1);
	ge_cairo_set_color (cr, &border);
	cairo_stroke (cr);
}

static void
clearlooks_draw_scrollbar_stepper (cairo_t *cr,
				   GtkThemingEngine                 *engine,
                                   const ScrollBarParameters        *scrollbar,
                                   const ScrollBarStepperParameters *stepper,
                                   int x, int y, int width, int height)
{
	ClearlooksStyleFunctions *style_functions;
	CairoCorners corners = CR_CORNER_NONE;
	CairoColor   shade, border, *bg_color;
	CairoColor   s1, s2, s3, s4;
	cairo_pattern_t *pattern;
	GtkStateFlags state;
	gint radius;

	state = gtk_theming_engine_get_state (engine);
	gtk_theming_engine_get (engine, state,
				"border-radius", &radius,
				"background-color", &bg_color,
				NULL);

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);

	radius = MIN (radius, MIN ((width - 2.0) / 2.0, (height - 2.0) / 2.0));
	clearlooks_lookup_shade (engine, 6, (GdkRGBA *) &shade);

	ge_shade_color(&shade, 1.08, &border);

	if (scrollbar->horizontal)
	{
		if (stepper->stepper == CL_STEPPER_START)
			corners = CR_CORNER_TOPLEFT | CR_CORNER_BOTTOMLEFT;
		else if (stepper->stepper == CL_STEPPER_END)
			corners = CR_CORNER_TOPRIGHT | CR_CORNER_BOTTOMRIGHT;

		if (stepper->stepper == CL_STEPPER_START_INNER)
		{
			x -= 1;
			width += 1;
		}
		else if (stepper->stepper == CL_STEPPER_END_INNER)
		{
			width += 1;
		}
	}
	else
	{
		if (stepper->stepper == CL_STEPPER_START)
			corners = CR_CORNER_TOPLEFT | CR_CORNER_TOPRIGHT;
		else if (stepper->stepper == CL_STEPPER_END)
			corners = CR_CORNER_BOTTOMLEFT | CR_CORNER_BOTTOMRIGHT;

		if (stepper->stepper == CL_STEPPER_START_INNER)
		{
			y -= 1;
			height += 1;
		}
		else if (stepper->stepper == CL_STEPPER_END_INNER)
		{
			height += 1;
		}
	}

	cairo_translate (cr, x, y);
	cairo_set_line_width (cr, 1);

	ge_cairo_rounded_rectangle (cr, 1, 1, width-2, height-2, radius, corners);

	if (scrollbar->horizontal)
		pattern = cairo_pattern_create_linear (0, 0, 0, height);
	else
		pattern = cairo_pattern_create_linear (0, 0, width, 0);

	ge_shade_color (bg_color, SHADE_TOP, &s1);
	ge_shade_color (bg_color, SHADE_CENTER_TOP, &s2);
	ge_shade_color (bg_color, SHADE_CENTER_BOTTOM, &s3);
	ge_shade_color (bg_color, SHADE_BOTTOM, &s4);

	cairo_pattern_add_color_stop_rgb(pattern, 0,   s1.r, s1.g, s1.b);
	cairo_pattern_add_color_stop_rgb(pattern, 0.3, s2.r, s2.g, s2.b);
	cairo_pattern_add_color_stop_rgb(pattern, 0.7, s3.r, s3.g, s3.b);
	cairo_pattern_add_color_stop_rgb(pattern, 1.0, s4.r, s4.g, s4.b);
	cairo_set_source (cr, pattern);
	cairo_fill (cr);
	cairo_pattern_destroy (pattern);

	style_functions->draw_top_left_highlight (cr, &s2, engine, 1, 1, width - 2, height - 2);

	ge_cairo_inner_rounded_rectangle (cr, 0, 0, width, height, radius, corners);
	clearlooks_set_border_gradient (cr, &border, 1.1, (scrollbar->horizontal ? 0 : width), (scrollbar->horizontal ? height: 0));
	cairo_stroke (cr);

	gdk_rgba_free ((GdkRGBA *) bg_color);
}

static void
clearlooks_draw_scrollbar_slider (cairo_t *cr,
				  GtkThemingEngine                *engine,
				  const ScrollBarParameters       *scrollbar,
				  int x, int y, int width, int height)
{
	ClearlooksStyleConstants *style_constants;
	GtkStateFlags state;

	cairo_save (cr);

	state = gtk_theming_engine_get_state (engine);
	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     NULL, &style_constants);

	if (scrollbar->junction & CL_JUNCTION_BEGIN)
	{
		if (scrollbar->horizontal)
		{
			x -= 1;
			width += 1;
		}
		else
		{
			y -= 1;
			height += 1;
		}
	}
	if (scrollbar->junction & CL_JUNCTION_END)
	{
		if (scrollbar->horizontal)
			width += 1;
		else
			height += 1;
	}

	if (!scrollbar->horizontal)
		ge_cairo_exchange_axis (cr, &x, &y, &width, &height);

	cairo_translate (cr, x, y);

	if (scrollbar->has_color)
	{
		CairoColor  border;
		CairoColor  fill    = scrollbar->color;
		CairoColor  hilight;
		CairoColor  shade1, shade2, shade3;
		cairo_pattern_t *pattern;

		clearlooks_lookup_shade (engine, 7, (GdkRGBA *) &border);

		if (state & GTK_STATE_FLAG_PRELIGHT)
			ge_shade_color (&fill, 1.1, &fill);

		cairo_set_line_width (cr, 1);

		ge_shade_color (&fill, 1.3, &hilight);
		ge_shade_color (&fill, 1.1, &shade1);
		ge_shade_color (&fill, 1.05, &shade2);
		ge_shade_color (&fill, 0.98, &shade3);

		pattern = cairo_pattern_create_linear (1, 1, 1, height-2);
		cairo_pattern_add_color_stop_rgb (pattern, 0,   shade1.r, shade1.g, shade1.b);
		cairo_pattern_add_color_stop_rgb (pattern, 0.5,	shade2.r, shade2.g, shade2.b);
		cairo_pattern_add_color_stop_rgb (pattern, 0.5,	shade3.r, shade3.g, shade3.b);
		cairo_pattern_add_color_stop_rgb (pattern, 1, 	fill.r,  fill.g,  fill.b);
		cairo_rectangle (cr, 1, 1, width-2, height-2);
		cairo_set_source (cr, pattern);
		cairo_fill (cr);
		cairo_pattern_destroy (pattern);

		cairo_set_source_rgba (cr, hilight.r, hilight.g, hilight.b, 0.5);
		ge_cairo_stroke_rectangle (cr, 1.5, 1.5, width-3, height-3);

		ge_cairo_set_color (cr, &border);
		ge_cairo_stroke_rectangle (cr, 0.5, 0.5, width-1, height-1);
	}
	else
	{
		CairoColor *bg_color, shade, light, dark, border;
		CairoColor s1, s2, s3, s4, s5;
		cairo_pattern_t *pattern;
		int bar_x, i;

		clearlooks_lookup_shade (engine, 4, (GdkRGBA *) &dark);
		clearlooks_lookup_shade (engine, 0, (GdkRGBA *) &light);
		clearlooks_lookup_shade (engine, 6, (GdkRGBA *) &shade);

		gtk_theming_engine_get (engine, state,
					"background-color", &bg_color,
					NULL);

		ge_shade_color (&shade, 1.08, &border);
		ge_shade_color (bg_color, SHADE_TOP, &s1);
		ge_shade_color (bg_color, SHADE_CENTER_TOP, &s2);
		ge_shade_color (bg_color, SHADE_CENTER_BOTTOM, &s3);
		ge_shade_color (bg_color, SHADE_BOTTOM, &s4);

		pattern = cairo_pattern_create_linear(1, 1, 1, height-1);
		cairo_pattern_add_color_stop_rgb(pattern, 0,   s1.r, s1.g, s1.b);
		cairo_pattern_add_color_stop_rgb(pattern, 0.3, s2.r, s2.g, s2.b);
		cairo_pattern_add_color_stop_rgb(pattern, 0.7, s3.r, s3.g, s3.b);
		cairo_pattern_add_color_stop_rgb(pattern, 1.0, s4.r, s4.g, s4.b);

		cairo_rectangle (cr, 1, 1, width-2, height-2);
		cairo_set_source(cr, pattern);
		cairo_fill(cr);
		cairo_pattern_destroy(pattern);

		clearlooks_set_border_gradient (cr, &border, 1.1, 0, height);
		ge_cairo_stroke_rectangle (cr, 0.5, 0.5, width-1, height-1);

		cairo_move_to (cr, 1.5, height-1.5);
		cairo_line_to (cr, 1.5, 1.5);
		cairo_line_to (cr, width-1.5, 1.5);
		ge_shade_color (&s2, style_constants->topleft_highlight_shade, &s5);
		s5.a = style_constants->topleft_highlight_alpha;
		ge_cairo_set_color (cr, &s5);
		cairo_stroke(cr);

		/* draw handles */
		cairo_set_line_width (cr, 1);
		cairo_set_line_cap (cr, CAIRO_LINE_CAP_BUTT);

		bar_x = width/2 - 4;

		for (i=0; i<3; i++)
		{
			cairo_move_to (cr, bar_x + 0.5, 4);
			cairo_line_to (cr, bar_x + 0.5, height-4);
			ge_cairo_set_color (cr, &dark);
			cairo_stroke (cr);

			cairo_move_to (cr, bar_x+1.5, 4);
			cairo_line_to (cr, bar_x+1.5, height-4);
			ge_cairo_set_color (cr, &light);
			cairo_stroke (cr);

			bar_x += 3;
		}

		gdk_rgba_free ((GdkRGBA *) bg_color);
	}

	cairo_restore (cr);
}

static void
clearlooks_draw_statusbar (cairo_t *cr,
			   GtkThemingEngine *engine,
                           int x, int y, int width, int height)
{
	CairoColor dark, hilight;

	clearlooks_lookup_shade (engine, 3, (GdkRGBA *) &dark);
	ge_shade_color (&dark, 1.4, &hilight);

	cairo_set_line_width  (cr, 1);
	cairo_translate       (cr, x, y+0.5);
	cairo_move_to         (cr, 0, 0);
	cairo_line_to         (cr, width, 0);
	ge_cairo_set_color    (cr, &dark);
	cairo_stroke          (cr);

	cairo_translate       (cr, 0, 1);
	cairo_move_to         (cr, 0, 0);
	cairo_line_to         (cr, width, 0);
	ge_cairo_set_color    (cr, &hilight);
	cairo_stroke          (cr);
}

static void
clearlooks_draw_menu_frame (cairo_t *cr,
			    GtkThemingEngine *engine,
                            int x, int y, int width, int height)
{
	CairoColor border;

	clearlooks_lookup_shade (engine, 5, (GdkRGBA *) &border);

	cairo_translate      (cr, x, y);
	cairo_set_line_width (cr, 1);

	ge_cairo_set_color (cr, &border);
	ge_cairo_stroke_rectangle (cr, 0.5, 0.5, width-1, height-1);
}

static void
clearlooks_draw_tooltip (cairo_t *cr,
			 GtkThemingEngine *engine,
                         int x, int y, int width, int height)
{
	CairoColor *bg_color, border;
	GtkStateFlags state;

	state = gtk_theming_engine_get_state (engine);
	gtk_theming_engine_get (engine, state,
				"background-color", &bg_color,
				NULL);

	ge_shade_color (bg_color, 0.6, &border);

	cairo_save (cr);

	cairo_translate      (cr, x, y);
	cairo_set_line_width (cr, 1);

	ge_cairo_set_color (cr, bg_color);
	cairo_rectangle (cr, 0, 0, width, height);
	cairo_fill (cr);

	ge_cairo_set_color (cr, &border);
	ge_cairo_stroke_rectangle (cr, 0.5, 0.5, width-1, height-1);

	cairo_restore (cr);

	gdk_rgba_free ((GdkRGBA *) bg_color);
}

static void
clearlooks_draw_icon_view_item (cairo_t                  *cr,
				GtkThemingEngine         *engine,
	                        int x, int y, int width, int height)
{
	CairoColor *upper_color;
	CairoColor lower_color;
	cairo_pattern_t *pattern;
	GtkStateFlags state;
	gint radius;

	cairo_save (cr);

	cairo_translate (cr, x, y);

	state = gtk_theming_engine_get_state (engine);
	gtk_theming_engine_get (engine, state,
				"border-radius", &radius,
				NULL);

	if (state & GTK_STATE_FLAG_FOCUSED)
		gtk_theming_engine_get (engine, state, "background-color", &upper_color, NULL);
	else
		gtk_theming_engine_get (engine, GTK_STATE_FLAG_ACTIVE, "background-color", &upper_color, NULL);

	ge_shade_color(upper_color, 0.92, &lower_color);

	pattern = cairo_pattern_create_linear (0, 0, 0, height);
	cairo_pattern_add_color_stop_rgb (pattern, 0.0, upper_color->r,
	                                                upper_color->g,
	                                                upper_color->b);
	cairo_pattern_add_color_stop_rgb (pattern, 1.0, lower_color.r,
	                                                lower_color.g,
	                                                lower_color.b);

	cairo_set_source (cr, pattern);
	ge_cairo_rounded_rectangle  (cr, 0, 0, width, height, radius, CR_CORNER_ALL);
	cairo_fill       (cr);

	cairo_pattern_destroy (pattern);

	cairo_restore (cr);

	gdk_rgba_free ((GdkRGBA *) upper_color);
}

static void
clearlooks_draw_handle (cairo_t *cr,
			GtkThemingEngine                *engine,
                        const HandleParameters          *handle,
                        int x, int y, int width, int height)
{
	ClearlooksStyleFunctions *style_functions;
	const CairoColor *fill;
	int num_bars = 6; /* shut up gcc warnings */
	GtkStateFlags state;

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);

	state = gtk_theming_engine_get_state (engine);
	gtk_theming_engine_get (engine, state,
				"background-color", &fill,
				NULL);

	cairo_save (cr);

	switch (handle->type)
	{
		case CL_HANDLE_TOOLBAR:
			num_bars    = 6;
		break;
		case CL_HANDLE_SPLITTER:
			num_bars    = 16;
		break;
	}

	if (state & GTK_STATE_FLAG_PRELIGHT)
	{
		cairo_rectangle (cr, x, y, width, height);
		ge_cairo_set_color (cr, fill);
		cairo_fill (cr);
	}

	cairo_translate (cr, x, y);

	cairo_set_line_width (cr, 1);

	if (handle->horizontal)
	{
		style_functions->draw_gripdots (cr, engine, 0, 0, width, height, num_bars, 2, 0.1);
	}
	else
	{
		style_functions->draw_gripdots (cr, engine, 0, 0, width, height, 2, num_bars, 0.1);
	}

	cairo_restore (cr);

	gdk_rgba_free ((GdkRGBA *) fill);
}

static void
clearlooks_draw_resize_grip (cairo_t *cr,
			     GtkThemingEngine                *engine,
                             const ResizeGripParameters      *grip,
                             int x, int y, int width, int height)
{
	CairoColor dark, hilight;
	int lx, ly;
	int x_down;
	int y_down;
	int dots;

	clearlooks_lookup_shade (engine, 4, (GdkRGBA *) &dark);
	ge_shade_color (&dark, 1.5, &hilight);

	/* The number of dots fitting into the area. Just hardcoded to 4 right now. */
	/* dots = MIN (width - 2, height - 2) / 3; */
	dots = 4;

	cairo_save (cr);

	switch (grip->edge)
	{
		case CL_WINDOW_EDGE_NORTH_EAST:
			x_down = 0;
			y_down = 0;
			cairo_translate (cr, x + width - 3*dots + 2, y + 1);
		break;
		case CL_WINDOW_EDGE_SOUTH_EAST:
			x_down = 0;
			y_down = 1;
			cairo_translate (cr, x + width - 3*dots + 2, y + height - 3*dots + 2);
		break;
		case CL_WINDOW_EDGE_SOUTH_WEST:
			x_down = 1;
			y_down = 1;
			cairo_translate (cr, x + 1, y + height - 3*dots + 2);
		break;
		case CL_WINDOW_EDGE_NORTH_WEST:
			x_down = 1;
			y_down = 0;
			cairo_translate (cr, x + 1, y + 1);
		break;
		default:
			/* Not implemented. */
			return;
	}

	for (lx = 0; lx < dots; lx++) /* horizontally */
	{
		for (ly = 0; ly <= lx; ly++) /* vertically */
		{
			int mx, my;
			mx = x_down * dots + (1 - x_down * 2) * lx - x_down;
			my = y_down * dots + (1 - y_down * 2) * ly - y_down;

			ge_cairo_set_color (cr, &hilight);
			cairo_rectangle (cr, mx*3-1, my*3-1, 2, 2);
			cairo_fill (cr);

			ge_cairo_set_color (cr, &dark);
			cairo_rectangle (cr, mx*3-1, my*3-1, 1, 1);
			cairo_fill (cr);
		}
	}

	cairo_restore (cr);
}

static void
clearlooks_draw_radiobutton (cairo_t *cr,
			     GtkThemingEngine         *engine,
                             const CheckboxParameters *checkbox,
                             int x, int y, int width, int height)
{
	CairoColor *bg_color, border, dot;
	CairoColor shadow;
	CairoColor highlight;
	cairo_pattern_t *pt;
	gboolean inconsistent;
	gboolean draw_bullet;
	gdouble w, h, cx, cy, radius;
	GtkStateFlags state;

	w = (gdouble) width;
	h = (gdouble) height;
	cx = width / 2.0;
	cy = height / 2.0;
	radius = MIN (width, height) / 2.0;

	cairo_save (cr);

	state = gtk_theming_engine_get_state (engine);

	gtk_theming_engine_get (engine, state,
				"background-color", &bg_color,
				NULL);

	inconsistent = (state & GTK_STATE_FLAG_INCONSISTENT) != 0;
	draw_bullet = (state & GTK_STATE_FLAG_ACTIVE) != 0;
	draw_bullet |= inconsistent;

	if (state & GTK_STATE_FLAG_INSENSITIVE)
	{
		clearlooks_lookup_shade (engine, 5, (GdkRGBA *) &border);
		clearlooks_lookup_shade (engine, 6, (GdkRGBA *) &dot);
	}
	else
	{
		CairoColor *color;

		clearlooks_lookup_shade (engine, 6, (GdkRGBA *) &border);

		gtk_theming_engine_get (engine, state,
					"color", &color,
					NULL);
		dot = *color;
		gdk_rgba_free ((GdkRGBA *) color);
	}

	ge_shade_color (bg_color, 0.9, &shadow);
	ge_shade_color (bg_color, 1.1, &highlight);

	pt = cairo_pattern_create_linear (0, 0, radius * 2.0, radius * 2.0);
	cairo_pattern_add_color_stop_rgb (pt, 0.0, shadow.r, shadow.b, shadow.g);
	cairo_pattern_add_color_stop_rgba (pt, 0.5, shadow.r, shadow.b, shadow.g, 0.5);
	cairo_pattern_add_color_stop_rgba (pt, 0.5, highlight.r, highlight.g, highlight.b, 0.5);
	cairo_pattern_add_color_stop_rgb (pt, 1.0, highlight.r, highlight.g, highlight.b);

	cairo_translate (cr, x, y);

	cairo_set_line_width (cr, MAX (1.0, floor (radius/3)));
	cairo_arc (cr, ceil (cx), ceil (cy), floor (radius - 0.1), 0, G_PI*2);
	cairo_set_source (cr, pt);
	cairo_stroke (cr);
	cairo_pattern_destroy (pt);

	cairo_set_line_width (cr, MAX (1.0, floor (radius/6)));

	cairo_arc (cr, ceil (cx), ceil (cy), MAX (1.0, ceil (radius) - 1.5), 0, G_PI*2);

	if ((state & GTK_STATE_FLAG_INSENSITIVE) == 0)
	{
		ge_cairo_set_color (cr, bg_color);
		cairo_fill_preserve (cr);
	}

	ge_cairo_set_color (cr, &border);
	cairo_stroke (cr);

	if (draw_bullet)
	{
		if (inconsistent)
		{
			gdouble line_width = floor (radius * 2 / 3);

			cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
			cairo_set_line_width (cr, line_width);

			cairo_move_to (cr, ceil (cx - radius/3.0 - line_width) + line_width, ceil (cy - line_width) + line_width);
			cairo_line_to (cr, floor (cx + radius/3.0 + line_width) - line_width, ceil (cy - line_width) + line_width);

			ge_cairo_set_color (cr, &dot);
			cairo_stroke (cr);
		}
		else
		{
			cairo_arc (cr, ceil (cx), ceil (cy), floor (radius/2.0), 0, G_PI*2);
			ge_cairo_set_color (cr, &dot);
			cairo_fill (cr);

			cairo_arc (cr, floor (cx - radius/10.0), floor (cy - radius/10.0), floor (radius/6.0), 0, G_PI*2);
			cairo_set_source_rgba (cr, highlight.r, highlight.g, highlight.b, 0.5);
			cairo_fill (cr);
		}
	}

	cairo_restore (cr);
	gdk_rgba_free ((GdkRGBA *) bg_color);
}

static void
clearlooks_draw_checkbox (cairo_t *cr,
			  GtkThemingEngine         *engine,
                          const CheckboxParameters *checkbox,
                          int x, int y, int width, int height)
{
	CairoColor *bg_color, border, dot;
	gboolean inconsistent = FALSE;
	gboolean draw_bullet;
	GtkStateFlags state;
	gint radius;

	state = gtk_theming_engine_get_state (engine);
	inconsistent = (state & GTK_STATE_FLAG_INCONSISTENT) != 0;
	draw_bullet = (state & GTK_STATE_FLAG_ACTIVE);
	draw_bullet |= inconsistent;

	cairo_save (cr);

	gtk_theming_engine_get (engine, state,
				"border-radius", &radius,
				"background-color", &bg_color,
				NULL);

	if (state & GTK_STATE_FLAG_INSENSITIVE)
	{
		clearlooks_lookup_shade (engine, 5, (GdkRGBA *) &border);
		clearlooks_lookup_shade (engine, 6, (GdkRGBA *) &dot);
	}
	else
	{
		CairoColor *color;

		clearlooks_lookup_shade (engine, 6, (GdkRGBA *) &border);

		gtk_theming_engine_get (engine, state,
					"color", &color,
					NULL);

		dot = *color;
		gdk_rgba_free ((GdkRGBA *) color);
	}

	cairo_translate (cr, x, y);
	cairo_set_line_width (cr, 1);

#if 0
	if (widget->xthickness >= 3 && widget->ythickness >= 3)
	{
		widget->style_functions->draw_inset (cr, &widget->parentbg, 0, 0, width, height, 1, CR_CORNER_ALL);

		/* Draw the rectangle for the checkbox itself */
		ge_cairo_rounded_rectangle (cr, 1.5, 1.5, width-3, height-3, (radius > 0)? 1 : 0, CR_CORNER_ALL);
	}
	else
#endif
	{
		/* Draw the rectangle for the checkbox itself */
		ge_cairo_rounded_rectangle (cr, 0.5, 0.5, width-1, height-1, (radius > 0)? 1 : 0, CR_CORNER_ALL);
	}

	if ((state & GTK_STATE_FLAG_INSENSITIVE) == 0)
	{
		ge_cairo_set_color (cr, bg_color);
		cairo_fill_preserve (cr);
	}

	ge_cairo_set_color (cr, &border);
	cairo_stroke (cr);

	if (draw_bullet)
	{
		if (inconsistent) /* Inconsistent */
		{
			cairo_set_line_width (cr, 2.0);
			cairo_move_to (cr, 3, height*0.5);
			cairo_line_to (cr, width-3, height*0.5);
		}
		else
		{
			cairo_set_line_width (cr, 1.7);
			cairo_move_to (cr, 0.5 + (width*0.2), (height*0.5));
			cairo_line_to (cr, 0.5 + (width*0.4), (height*0.7));

			cairo_curve_to (cr, 0.5 + (width*0.4), (height*0.7),
			                    0.5 + (width*0.5), (height*0.4),
			                    0.5 + (width*0.70), (height*0.25));

		}

		ge_cairo_set_color (cr, &dot);
		cairo_stroke (cr);
	}

	cairo_restore (cr);

	gdk_rgba_free ((GdkRGBA *) bg_color);
}

static void
clearlooks_draw_normal_arrow (cairo_t *cr, const CairoColor *color,
                              double x, double y, double width, double height)
{
	double arrow_width;
	double arrow_height;
	double line_width_2;

	cairo_save (cr);

	arrow_width = MIN (height * 2.0 + MAX (1.0, ceil (height * 2.0 / 6.0 * 2.0) / 2.0) / 2.0, width);
	line_width_2 = MAX (1.0, ceil (arrow_width / 6.0 * 2.0) / 2.0) / 2.0;
	arrow_height = arrow_width / 2.0 + line_width_2;

	cairo_translate (cr, x, y - arrow_height / 2.0);

	cairo_move_to (cr, -arrow_width / 2.0, line_width_2);
	cairo_line_to (cr, -arrow_width / 2.0 + line_width_2, 0);
	/* cairo_line_to (cr, 0, arrow_height - line_width_2); */
	cairo_arc_negative (cr, 0, arrow_height - 2*line_width_2 - 2*line_width_2 * sqrt(2), 2*line_width_2, G_PI_2 + G_PI_4, G_PI_4);
	cairo_line_to (cr, arrow_width / 2.0 - line_width_2, 0);
	cairo_line_to (cr, arrow_width / 2.0, line_width_2);
	cairo_line_to (cr, 0, arrow_height);
	cairo_close_path (cr);

	ge_cairo_set_color (cr, color);
	cairo_fill (cr);

	cairo_restore (cr);
}

static void
clearlooks_draw_combo_arrow (cairo_t *cr, const CairoColor *color,
                             double x, double y, double width, double height)
{
	double arrow_width = MIN (height * 2 / 3.0, width);
	double arrow_height = arrow_width / 2.0;
	double gap_size = 1.0 * arrow_height;

	cairo_save (cr);
	cairo_translate (cr, x, y - (arrow_height + gap_size) / 2.0);
	cairo_rotate (cr, G_PI);
	clearlooks_draw_normal_arrow (cr, color, 0, 0, arrow_width, arrow_height);
	cairo_restore (cr);

	clearlooks_draw_normal_arrow (cr, color, x, y + (arrow_height + gap_size) / 2.0, arrow_width, arrow_height);
}

static void
_clearlooks_draw_arrow (cairo_t *cr, const CairoColor *color,
                        gdouble angle, ClearlooksArrowType type,
                        double x, double y, double width, double height)
{
	cairo_save (cr);

	if (type == CL_ARROW_NORMAL)
	{
		cairo_translate (cr, x, y);
		cairo_rotate (cr, angle - G_PI);
		clearlooks_draw_normal_arrow (cr, color, 0, 0, width, height);
	}
	else if (type == CL_ARROW_COMBO)
	{
		cairo_translate (cr, x, y);
		clearlooks_draw_combo_arrow (cr, color, 0, 0, width, height);
	}

	cairo_restore (cr);
}

static void
clearlooks_draw_arrow (cairo_t *cr,
		       GtkThemingEngine       *engine,
                       const ArrowParameters  *arrow,
                       int x, int y, int width, int height)
{
	GtkStateFlags state;
	CairoColor *color;
	gdouble tx, ty;

	state = gtk_theming_engine_get_state (engine);
	gtk_theming_engine_get (engine, state,
				"color", &color,
				NULL);
	tx = x + width/2.0;
	ty = y + height/2.0;

	if (state & GTK_STATE_FLAG_INSENSITIVE)
	{
		CairoColor shade;

                cairo_save (cr);
		clearlooks_lookup_shade (engine, 0, (GdkRGBA *) &shade);
		_clearlooks_draw_arrow (cr, &shade,
		                        arrow->angle, arrow->type,
		                        tx+0.5, ty+0.5, width, height);
                cairo_restore (cr);
	}

	_clearlooks_draw_arrow (cr, color, arrow->angle, arrow->type,
	                        tx, ty, width, height);

	gdk_rgba_free ((GdkRGBA *) color);
}

void
clearlooks_draw_focus (cairo_t *cr,
		       GtkThemingEngine       *engine,
                       const FocusParameters  *focus,
                       int x, int y, int width, int height)
{
	const GtkWidgetPath *path;

	path = gtk_theming_engine_get_path (engine);

	if (focus->has_color)
		ge_cairo_set_color (cr, &focus->color);
	else if (focus->type == CL_FOCUS_COLOR_WHEEL_LIGHT)
		cairo_set_source_rgb (cr, 0., 0., 0.);
	else if (focus->type == CL_FOCUS_COLOR_WHEEL_DARK)
		cairo_set_source_rgb (cr, 1., 1., 1.);
	else
	{
		GdkRGBA *color;

		gtk_theming_engine_get (engine,
					gtk_theming_engine_get_state (engine),
					"color", &color,
					NULL);
		color->alpha = 0.7;
		gdk_cairo_set_source_rgba (cr, color);
		gdk_rgba_free (color);
	}

	cairo_set_line_width (cr, focus->line_width);

	if (focus->dash_list[0])
	{
		gint n_dashes = strlen ((gchar *)focus->dash_list);
		gdouble *dashes = g_new (gdouble, n_dashes);
		gdouble total_length = 0;
		gdouble dash_offset;
		gint i;

		for (i = 0; i < n_dashes; i++)
		{
			dashes[i] = focus->dash_list[i];
			total_length += focus->dash_list[i];
		}

		dash_offset = -focus->line_width / 2.0;
		while (dash_offset < 0)
			dash_offset += total_length;

		cairo_set_dash (cr, dashes, n_dashes, dash_offset);
		g_free (dashes);
	}

	cairo_rectangle (cr,
	                 x + focus->line_width / 2.0,
	                 y + focus->line_width / 2.0,
	                 width - focus->line_width, height - focus->line_width);
	cairo_stroke_preserve (cr);

	if (gtk_widget_path_is_type (path, GTK_TYPE_BUTTON)) {
		ge_cairo_set_color (cr, &focus->fill_color);
		cairo_fill (cr);
	}
}

void
clearlooks_set_mixed_color (cairo_t          *cr,
                            const CairoColor *color1,
                            const CairoColor *color2,
                            gdouble mix_factor)
{
	CairoColor composite;

	ge_mix_color (color1, color2, mix_factor, &composite);
	ge_cairo_set_color (cr, &composite);
}

void
clearlooks_lookup_shade (GtkThemingEngine *engine,
			 guint             shade_number,
			 GdkRGBA          *color)
{
	gdouble shades[] = { 1.15, 0.95, 0.896, 0.82, 0.7, 0.665, 0.475, 0.45, 0.4 };
	gdouble contrast, shade;
	GdkRGBA *bg_color;

	if (shade_number >= G_N_ELEMENTS (shades)) {
		shade_number = G_N_ELEMENTS (shades) - 1;
	}

	gtk_theming_engine_get (engine, 0,
				"-clearlooks-contrast", &contrast,
				"background-color", &bg_color,
				NULL);

	shade = shades[shade_number];

	/* Lighter to darker */
	ge_shade_color ((CairoColor *) bg_color,
			(shade < 1.0) ? (shade / contrast) : (shade * contrast),
			(CairoColor *) color);

	gdk_rgba_free ((GdkRGBA *) bg_color);
}

void
clearlooks_lookup_spot (GtkThemingEngine *engine,
			guint             spot_number,
			GdkRGBA          *color)
{
	gdouble spots[] = { 1.25, 1.05, 0.65 };
	GdkRGBA *bg_color;

	if (spot_number >= G_N_ELEMENTS (spots)) {
		spot_number = G_N_ELEMENTS (spots) - 1;
	}

	gtk_theming_engine_get (engine,
				GTK_STATE_FLAG_ACTIVE,
				"background-color", &bg_color,
				NULL);

	/* Lighter to darker */
	ge_shade_color ((CairoColor *) bg_color, spots[spot_number], (CairoColor *) color);

	gdk_rgba_free ((GdkRGBA *) bg_color);
}

CairoCorners
clearlooks_get_corners (GtkThemingEngine *engine)
{
	GtkJunctionSides sides;
	CairoCorners corners = CR_CORNER_ALL;

	sides = gtk_theming_engine_get_junction_sides (engine);

	if (sides == 0)
		return CR_CORNER_ALL;

	if (sides & GTK_JUNCTION_LEFT)
		corners &= ~(CR_CORNER_TOPLEFT | CR_CORNER_BOTTOMLEFT);
	if (sides & GTK_JUNCTION_RIGHT)
		corners &= ~(CR_CORNER_TOPRIGHT | CR_CORNER_BOTTOMRIGHT);
	if (sides & GTK_JUNCTION_BOTTOM)
		corners &= ~(CR_CORNER_BOTTOMLEFT | CR_CORNER_BOTTOMRIGHT);
	if (sides & GTK_JUNCTION_TOP)
		corners &= ~(CR_CORNER_TOPLEFT | CR_CORNER_TOPRIGHT);

	return corners;
}

void
clearlooks_lookup_functions (ClearlooksEngine          *engine,
			     ClearlooksStyleFunctions **functions,
			     ClearlooksStyleConstants **constants)
{
	ClearlooksStyle style;

	gtk_theming_engine_get (GTK_THEMING_ENGINE (engine), 0,
				"-clearlooks-style", &style,
				NULL);

	if (functions)
		*functions = &engine->style_functions[style];

	if (constants)
		*constants = &engine->style_constants[style];
}

void
clearlooks_register_style_classic (ClearlooksStyleFunctions *functions, ClearlooksStyleConstants *constants)
{
	g_assert (functions);

	functions->draw_top_left_highlight  = clearlooks_draw_top_left_highlight;
	functions->draw_button              = clearlooks_draw_button;
	functions->draw_scale_trough        = clearlooks_draw_scale_trough;
	functions->draw_progressbar_trough  = clearlooks_draw_progressbar_trough;
	functions->draw_progressbar_fill    = clearlooks_draw_progressbar_fill;
	functions->draw_slider_button       = clearlooks_draw_slider_button;
	functions->draw_entry               = clearlooks_draw_entry;
	functions->draw_entry_progress      = clearlooks_draw_entry_progress;
	functions->draw_spinbutton          = clearlooks_draw_spinbutton;
	functions->draw_spinbutton_down     = clearlooks_draw_spinbutton_down;
	functions->draw_optionmenu          = clearlooks_draw_optionmenu;
	functions->draw_inset               = clearlooks_draw_inset;
	functions->draw_menubar	            = clearlooks_draw_menubar;
	functions->draw_tab                 = clearlooks_draw_tab;
	functions->draw_frame               = clearlooks_draw_frame;
	functions->draw_separator           = clearlooks_draw_separator;
	functions->draw_menu_item_separator = clearlooks_draw_menu_item_separator;
	functions->draw_list_view_header    = clearlooks_draw_list_view_header;
	functions->draw_toolbar             = clearlooks_draw_toolbar;
	functions->draw_menuitem            = clearlooks_draw_menuitem;
	functions->draw_menubaritem         = clearlooks_draw_menubaritem;
	functions->draw_selected_cell       = clearlooks_draw_selected_cell;
	functions->draw_scrollbar_stepper   = clearlooks_draw_scrollbar_stepper;
	functions->draw_scrollbar_slider    = clearlooks_draw_scrollbar_slider;
	functions->draw_scrollbar_trough    = clearlooks_draw_scrollbar_trough;
	functions->draw_statusbar           = clearlooks_draw_statusbar;
	functions->draw_menu_frame          = clearlooks_draw_menu_frame;
	functions->draw_tooltip             = clearlooks_draw_tooltip;
	functions->draw_icon_view_item      = clearlooks_draw_icon_view_item;
	functions->draw_handle              = clearlooks_draw_handle;
	functions->draw_resize_grip         = clearlooks_draw_resize_grip;
	functions->draw_arrow               = clearlooks_draw_arrow;
	functions->draw_focus               = clearlooks_draw_focus;
	functions->draw_checkbox            = clearlooks_draw_checkbox;
	functions->draw_radiobutton         = clearlooks_draw_radiobutton;
	functions->draw_shadow              = clearlooks_draw_shadow;
	functions->draw_slider              = clearlooks_draw_slider;
	functions->draw_gripdots            = clearlooks_draw_gripdots;

	constants->topleft_highlight_shade  = 1.3;
	constants->topleft_highlight_alpha  = 0.6;
}
