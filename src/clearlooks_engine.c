/* Clearlooks - a cairo based GTK+ engine
 * Copyright (C) 2005 Richard Stellingwerff <remenic@gmail.com>
 * Copyright (C) 2007 Benjamin Berg <benjamin@sipsolutions.net>
 * Copyright (C) 2007-2008 Andrea Cimitan <andrea.cimitan@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Project contact: <gnome-themes-list@gnome.org>
 *
 */


#include <gtk/gtk.h>
#include <cairo.h>
#include <math.h>
#include <string.h>

#include <ge-support.h>
#include "clearlooks_typebuiltin.h"
#include "clearlooks_engine.h"
#include "clearlooks_draw.h"
#include "support.h"

/* #define DEBUG 1 */

#define CLEARLOOKS_NAMESPACE "clearlooks"
#define DETAIL(xx)   ((detail) && (!strcmp(xx, detail)))
#define CHECK_HINT(xx) (ge_check_hint ((xx), CLEARLOOKS_RC_STYLE ((style)->rc_style)->hint, widget))

#define DRAW_ARGS    GtkStyle       *style, \
                     cairo_t        *cr, \
                     GtkStateType    state_type, \
                     GtkShadowType   shadow_type, \
                     GtkWidget      *widget, \
                     const gchar    *detail, \
                     gint            x, \
                     gint            y, \
                     gint            width, \
                     gint            height

#ifdef HAVE_ANIMATION
#include "animation.h"
#endif

#define STYLE_FUNCTION(function) (CLEARLOOKS_ENGINE_GET_CLASS (style)->style_functions[CLEARLOOKS_STYLE (style)->style].function)

G_DEFINE_DYNAMIC_TYPE (ClearlooksEngine, clearlooks_engine, GTK_TYPE_THEMING_ENGINE)

static void
clearlooks_engine_render_background (GtkThemingEngine *engine,
				     cairo_t          *cr,
				     gdouble           x,
				     gdouble           y,
				     gdouble           width,
				     gdouble           height)
{
	ClearlooksStyleFunctions  *style_functions;
	ClearlooksStyle style;
	GtkStateFlags state;

	GE_CAIRO_INIT

	state = gtk_theming_engine_get_state (engine);
	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);
	gtk_theming_engine_get (engine, state,
				"-clearlooks-style", &style,
				NULL);

	if ((state & GTK_STATE_FLAG_SELECTED) &&
	    gtk_theming_engine_has_class (engine, "cell"))
	{
		/* XXX: We could expose the side details by setting params->corners accordingly
		 *      or adding another option. */
		style_functions->draw_selected_cell (cr, engine, x, y, width, height);
	}
	else if (gtk_theming_engine_has_class (engine, "tooltip"))
	{
		style_functions->draw_tooltip (cr, engine, x, y, width, height);
	}
	else if (gtk_theming_engine_has_class (engine, "cell") &&
		 gtk_theming_engine_has_class (engine, "icon-view"))
	{
		style_functions->draw_icon_view_item (cr, engine, x, y, width, height);
	}
	else if ((state & GTK_STATE_FLAG_PRELIGHT) &&
		 (style == CLEARLOOKS_STYLE_GLOSSY || style == CLEARLOOKS_STYLE_GUMMY) &&
		 (gtk_theming_engine_has_class (engine, "check") || gtk_theming_engine_has_class (engine, "radio")))
	{
		/* XXX: Don't draw any check/radiobutton bg in GLOSSY or GUMMY mode. */
	}
	else
	{
		GTK_THEMING_ENGINE_CLASS (clearlooks_engine_parent_class)->render_background (engine, cr,
											      x, y, width, height);
	}
}

#if 0

static void
clearlooks_engine_render_shadow (DRAW_ARGS)
{
	ClearlooksStyle  *clearlooks_style = CLEARLOOKS_STYLE (style);
	ClearlooksColors *colors = &clearlooks_style->colors;

	CHECK_ARGS

	if (DETAIL ("frame") && CHECK_HINT (GE_HINT_STATUSBAR))
	{
		WidgetParameters params;

		clearlooks_set_widget_parameters (widget, style, state_type, &params);

		gtk_style_apply_default_background (style, cr, gtk_widget_get_window (widget),
                                                    state_type, x, y, width, height);
		if (shadow_type != GTK_SHADOW_NONE)
			STYLE_FUNCTION (draw_statusbar) (cr, colors, &params,
			                                 x, y, width, height);
	}
	else if (DETAIL ("frame") || DETAIL ("calendar"))
	{
		WidgetParameters params;
		FrameParameters  frame;
		frame.shadow  = shadow_type;
		frame.gap_x   = -1;  /* No gap will be drawn */
		frame.border  = &colors->shade[4];

		clearlooks_set_widget_parameters (widget, style, state_type, &params);
		params.corners = CR_CORNER_NONE;

		STYLE_FUNCTION(draw_frame) (cr, colors, &params, &frame,
		                            x, y, width, height);
	}
	else if (DETAIL ("scrolled_window") || DETAIL ("viewport") || detail == NULL)
	{
		CairoColor border;

		if (CLEARLOOKS_STYLE (style)->style == CL_STYLE_CLASSIC)
			ge_shade_color ((CairoColor*)&colors->bg[0], 0.78, &border);
		else
			border = colors->shade[5];

		cairo_rectangle (cr, x+0.5, y+0.5, width-1, height-1);
		ge_cairo_set_color (cr, &border);
		cairo_set_line_width (cr, 1);
		cairo_stroke (cr);
	}
	else
	{
		WidgetParameters params;
		FrameParameters frame;

		frame.shadow = shadow_type;
		frame.gap_x  = -1;
		frame.border = &colors->shade[5];
		clearlooks_set_widget_parameters (widget, style, state_type, &params);
		params.corners = CR_CORNER_ALL;

		STYLE_FUNCTION(draw_frame) (cr, colors, &params, &frame, x, y, width, height);
	}
}

#endif

static void
clearlooks_engine_render_frame_gap (GtkThemingEngine *engine,
				    cairo_t          *cr,
				    gdouble          x,
				    gdouble          y,
				    gdouble          width,
				    gdouble          height,
				    GtkPositionType  gap_side,
				    gdouble          xy0_gap,
				    gdouble          xy1_gap)
{
	ClearlooksStyleFunctions  *style_functions;

	GE_CAIRO_INIT

	cairo_save (cr);
	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);

	if (gtk_theming_engine_has_class (engine, "notebook"))
	{
		FrameParameters  frame;
		CairoColor *bg_color;
		GtkStateFlags state;
		gint radius;

		frame.gap_side  = gap_side;
		frame.gap_x     = xy0_gap;
		frame.gap_width = xy1_gap - xy0_gap;
		clearlooks_lookup_shade (engine, 5, (GdkRGBA *) &frame.border);

		state = gtk_theming_engine_get_state (engine);

		gtk_theming_engine_get (engine, state,
					"background-color", &bg_color,
					"border-radius", &radius,
					NULL);

#if 0
		clearlooks_get_notebook_tab_position (widget, &start, &end);

		params.corners = CR_CORNER_ALL;
		switch (gap_side)
		{
			case GTK_POS_TOP:
				if (ge_widget_is_ltr (widget))
				{
					if (start)
						params.corners ^= CR_CORNER_TOPLEFT;
					if (end)
						params.corners ^= CR_CORNER_TOPRIGHT;
				}
				else
				{
					if (start)
						params.corners ^= CR_CORNER_TOPRIGHT;
					if (end)
						params.corners ^= CR_CORNER_TOPLEFT;
				}
			break;
			case GTK_POS_BOTTOM:
				if (ge_widget_is_ltr (widget))
				{
					if (start)
						params.corners ^= CR_CORNER_BOTTOMLEFT;
					if (end)
						params.corners ^= CR_CORNER_BOTTOMRIGHT;
				}
				else
				{
					if (start)
						params.corners ^= CR_CORNER_BOTTOMRIGHT;
					if (end)
						params.corners ^= CR_CORNER_BOTTOMLEFT;
				}
			break;
			case GTK_POS_LEFT:
				if (start)
					params.corners ^= CR_CORNER_TOPLEFT;
				if (end)
					params.corners ^= CR_CORNER_BOTTOMLEFT;
			break;
			case GTK_POS_RIGHT:
				if (start)
					params.corners ^= CR_CORNER_TOPRIGHT;
				if (end)
					params.corners ^= CR_CORNER_BOTTOMRIGHT;
			break;
		}
#endif

		/* Fill the background with bg[NORMAL] */
		ge_cairo_rounded_rectangle (cr, x, y, width, height, radius,
                                            clearlooks_get_corners (engine));
		ge_cairo_set_color (cr, bg_color);
		cairo_fill (cr);

		style_functions->draw_frame (cr, engine, &frame,
					     x, y, width, height);

		gdk_rgba_free ((GdkRGBA *) bg_color);
	}
	else if (gtk_theming_engine_has_class (engine, "frame"))
	{
		FrameParameters  frame;

		frame.gap_side  = gap_side;
		frame.gap_x     = xy0_gap;
		frame.gap_width = xy1_gap - xy0_gap;
		clearlooks_lookup_shade (engine, 5, (GdkRGBA *) &frame.border);

		style_functions->draw_frame (cr, engine, &frame,
					     x, y, width, height);
	}
	else
	{
		GTK_THEMING_ENGINE_CLASS (clearlooks_engine_parent_class)->render_frame_gap (engine, cr, x, y, width, height,
		                                       gap_side, xy0_gap, xy1_gap);
	}

	cairo_restore (cr);
}

static void
clearlooks_engine_render_extension (GtkThemingEngine *engine,
				    cairo_t          *cr,
				    gdouble           x,
				    gdouble           y,
				    gdouble           width,
				    gdouble           height,
				    GtkPositionType   gap_side)
{
	ClearlooksStyleFunctions  *style_functions;

	GE_CAIRO_INIT

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);

	if (gtk_theming_engine_has_region (engine, GTK_STYLE_REGION_TAB, NULL))
	{
		TabParameters    tab;
		FocusParameters  focus;
		CairoColor      *bg_color, *focus_color;
		GtkStateFlags    state;

		state = gtk_theming_engine_get_state (engine);
		gtk_theming_engine_get (engine, state,
					"background-color", &bg_color,
					"-clearlooks-focus-color", &focus_color,
					NULL);

		tab.gap_side = (ClearlooksGapSide) gap_side;

#if 0
		switch (gap_side)
		{
			case CL_GAP_TOP:
				params.corners = CR_CORNER_BOTTOMLEFT | CR_CORNER_BOTTOMRIGHT;
				break;
			case CL_GAP_BOTTOM:
				params.corners = CR_CORNER_TOPLEFT | CR_CORNER_TOPRIGHT;
				break;
			case CL_GAP_LEFT:
				params.corners = CR_CORNER_TOPRIGHT | CR_CORNER_BOTTOMRIGHT;
				break;
			case CL_GAP_RIGHT:
				params.corners = CR_CORNER_TOPLEFT | CR_CORNER_BOTTOMLEFT;
				break;
		}
#endif

		/* Focus color */
		if (focus_color)
		{
			focus.color = *focus_color;
			focus.has_color = TRUE;
			gdk_rgba_free ((GdkRGBA *) focus_color);
		}
		else
			focus.color = *bg_color;

		tab.focus = focus;

		style_functions->draw_tab (cr, engine, &tab,
					   x, y, width, height);

		gdk_rgba_free ((GdkRGBA *) bg_color);
	}
	else
	{
		GTK_THEMING_ENGINE_CLASS (clearlooks_engine_parent_class)->render_extension (engine, cr, x, y, width, height, gap_side);
	}
}

static void
clearlooks_engine_render_handle (GtkThemingEngine *engine,
				 cairo_t          *cr,
				 gdouble           x,
				 gdouble           y,
				 gdouble           width,
				 gdouble           height)
{
	ClearlooksStyleFunctions *style_functions;

	GE_CAIRO_INIT

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);

	if (gtk_theming_engine_has_class (engine, "dock"))
	{
		HandleParameters handle;

		handle.type = CL_HANDLE_TOOLBAR;
		handle.horizontal = (width > height);

		style_functions->draw_handle (cr, engine, &handle,
					      x, y, width, height);
	}
	else if (gtk_theming_engine_has_class (engine, "paned"))
	{
		HandleParameters handle;

		handle.type = CL_HANDLE_SPLITTER;
		handle.horizontal = (width > height);

		style_functions->draw_handle (cr, engine, &handle,
					      x, y, width, height);
	}
	else if (gtk_theming_engine_has_class (engine, "grip"))
	{
		ResizeGripParameters grip;

		grip.edge = CL_WINDOW_EDGE_SOUTH_EAST; //(ClearlooksWindowEdge)edge;

		style_functions->draw_resize_grip (cr, engine, &grip,
						   x, y, width, height);
	}
	else
	{
		HandleParameters handle;

		handle.type = CL_HANDLE_TOOLBAR;
		handle.horizontal = (width > height);

		style_functions->draw_handle (cr, engine, &handle,
					      x, y, width, height);
	}
}

static void
clearlooks_engine_render_frame (GtkThemingEngine *engine,
				cairo_t          *cr,
				gdouble           x,
				gdouble           y,
				gdouble           width,
				gdouble           height)
{
	ClearlooksStyleFunctions *style_functions;
	const GtkWidgetPath *path;
	GtkTextDirection direction;
	GtkBorderStyle border_style;
	GtkStateFlags state;

	GE_CAIRO_INIT

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);
	state = gtk_theming_engine_get_state (engine);
	path = gtk_theming_engine_get_path (engine);
	direction = gtk_theming_engine_get_direction (engine);

	gtk_theming_engine_get (engine, state,
				"border-style", &border_style,
				NULL);

	if (border_style == GTK_BORDER_STYLE_NONE)
		return;

	if (gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_MENUBAR))
	{
		MenuBarParameters menubar;
		gboolean horizontal;

		menubar.style = 0; //clearlooks_style->menubarstyle;

		horizontal = height < 2*width;
		/* This is not that great. Ideally we would have a nice vertical menubar. */
		if (horizontal)
			style_functions->draw_menubar (cr, engine, &menubar,
						       x, y, width, height);
	}
	if (gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_ENTRY) &&
	    !gtk_widget_path_has_parent (path, GTK_TYPE_TREE_VIEW))
	{
		FocusParameters  focus;
		CairoColor *focus_color;

		gtk_theming_engine_get (engine, state,
					"-clearlooks-focus-color", &focus_color,
					NULL);

#if 0
		/* Override the entries state type, because we are too lame to handle this via
		 * the focus ring, and GtkEntry doesn't even set the INSENSITIVE state ... */
		if (state_type == GTK_STATE_NORMAL && widget && GE_IS_ENTRY (widget))
			state_type = gtk_widget_get_state (widget);

		if (CHECK_HINT (GE_HINT_COMBOBOX_ENTRY) || CHECK_HINT (GE_HINT_SPINBUTTON))
		{
			width += style->xthickness;
			if (!params.ltr)
				x -= style->xthickness;

			if (params.ltr)
				params.corners = CR_CORNER_TOPLEFT | CR_CORNER_BOTTOMLEFT;
			else
				params.corners = CR_CORNER_TOPRIGHT | CR_CORNER_BOTTOMRIGHT;
		}

		/* Fill the background as it is initilized to base[NORMAL].
		 * Relevant GTK+ bug: http://bugzilla.gnome.org/show_bug.cgi?id=513471
		 * The fill only happens if no hint has been added by some application
		 * that is faking GTK+ widgets. */
		if (!widget || !g_object_get_data(G_OBJECT (widget), "transparent-bg-hint"))
		{
			cairo_rectangle (cr, 0, 0, width, height);
			ge_cairo_set_color (cr, &params.parentbg);
			cairo_fill (cr);
		}
#endif

		/* Focus color */
		if (focus_color)
		{
			focus.color = *focus_color;
			focus.has_color = TRUE;
		}
		else
			clearlooks_lookup_spot (engine, 2, (GdkRGBA *) &focus.color);

		style_functions->draw_entry (cr, engine, &focus,
		                             x, y, width, height);

		gdk_rgba_free ((GdkRGBA *) focus_color);
	}
	else if (gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_BUTTON) &&
		 gtk_theming_engine_has_class (engine, GTK_STYLE_REGION_COLUMN_HEADER))
	{
		ListViewHeaderParameters header;

		gint columns, column_index;

		/* XXX: This makes unknown treeview header "middle", in need for something nicer */
		columns = 3;
		column_index = 1;

#if 0
		if (GE_IS_TREE_VIEW (gtk_widget_get_parent (widget)))
		{
			clearlooks_treeview_get_header_index (GTK_TREE_VIEW(gtk_widget_get_parent (widget)),
			                                      widget, &column_index, &columns,
			                                      &resizable);
		}

		header.resizable = resizable;

		header.order = 0;
		if (column_index == 0)
			header.order |= params.ltr ? CL_ORDER_FIRST : CL_ORDER_LAST;
		if (column_index == columns-1)
			header.order |= params.ltr ? CL_ORDER_LAST : CL_ORDER_FIRST;

		gtk_style_apply_default_background (style, cr, gtk_widget_get_window (widget), state_type, x, y, width, height);
#endif

		style_functions->draw_list_view_header (cr, engine, &header,
							x, y, width, height);
	}
	else if (gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_BUTTON))
	{
#if 0
		if (CHECK_HINT (GE_HINT_COMBOBOX_ENTRY))
		{
			if (params.ltr)
				params.corners = CR_CORNER_TOPRIGHT | CR_CORNER_BOTTOMRIGHT;
			else
				params.corners = CR_CORNER_TOPLEFT | CR_CORNER_BOTTOMLEFT;

			if (params.xthickness > 2)
			{
				if (params.ltr)
					x--;
				width++;
			}
		}
		else
		{
			params.corners = CR_CORNER_ALL;
			if (clearlooks_style->reliefstyle != 0)
				params.enable_shadow = TRUE;
		}
#endif

		style_functions->draw_button (cr, engine,
					      x, y, width, height);
	}
	else if (gtk_theming_engine_has_class (engine, "spinbutton") &&
		 gtk_theming_engine_has_class (engine, "button"))
	{
		if (state & GTK_STATE_FLAG_ACTIVE)
		{
			GtkJunctionSides sides;
			GtkTextDirection direction;

			sides = gtk_theming_engine_get_junction_sides (engine);
			direction = gtk_theming_engine_get_direction (engine);
#if 0
			if (style->xthickness == 3)
			{
				width++;
				if (params.ltr)
					x--;
			}

			if (sides & GTK_JUNCTION_BOTTOM)
			{
				height+=2;
				if (direction == GTK_TEXT_DIR_LTR)
					params.corners = CR_CORNER_TOPRIGHT;
				else
					params.corners = CR_CORNER_TOPLEFT;
			}
			else
			{
				if (direction == GTK_TEXT_DIR_LTR)
					params.corners = CR_CORNER_BOTTOMRIGHT;
				else
					params.corners = CR_CORNER_BOTTOMLEFT;
			}
#endif

			style_functions->draw_spinbutton_down (cr, engine, x, y, width, height);
		}
	}
	else if (gtk_theming_engine_has_class (engine, "spinbutton"))
	{
#if 0
		if (params.ltr)
			params.corners = CR_CORNER_TOPRIGHT | CR_CORNER_BOTTOMRIGHT;
		else
			params.corners = CR_CORNER_TOPLEFT | CR_CORNER_BOTTOMLEFT;

		if (style->xthickness == 3)
		{
			if (params.ltr)
				x--;
			width++;
		}
#endif

		style_functions->draw_spinbutton (cr, engine,
						  x, y, width, height);
	}
	else if (gtk_widget_path_is_type (path, GTK_TYPE_SCALE) &&
		 gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_TROUGH))
	{
		SliderParameters slider;

#if 0
		params.corners    = CR_CORNER_NONE;

		slider.lower = DETAIL ("trough-lower");
		slider.fill_level = DETAIL ("trough-fill-level") || DETAIL ("trough-fill-level-full");
#endif
		slider.lower = FALSE;
		slider.fill_level = gtk_theming_engine_has_class (engine, "scale");

#if 0
		if (CHECK_HINT (GE_HINT_HSCALE))
			slider.horizontal = TRUE;
		else if (CHECK_HINT (GE_HINT_VSCALE))
			slider.horizontal = FALSE;
		else /* Fallback based on the size... */
#endif
			slider.horizontal = width >= height;

		style_functions->draw_scale_trough (cr, engine, &slider,
						    x, y, width, height);
	}
	else if (gtk_widget_path_is_type (path, GTK_TYPE_PROGRESS_BAR) &&
		 gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_TROUGH))
	{
#if 0
		/* Fill the background as it is initilized to base[NORMAL].
		 * Relevant GTK+ bug: http://bugzilla.gnome.org/show_bug.cgi?id=513476
		 * The fill only happens if no hint has been added by some application
		 * that is faking GTK+ widgets. */
		if (!widget || !g_object_get_data(G_OBJECT (widget), "transparent-bg-hint"))
		{
			cairo_rectangle (cr, 0, 0, width, height);
			ge_cairo_set_color (cr, &params.parentbg);
			cairo_fill (cr);
		}
#endif
		style_functions->draw_progressbar_trough (cr, engine,
							  x, y, width, height);
	}
	else if (gtk_widget_path_is_type (path, GTK_TYPE_SCROLLBAR) &&
		 gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_TROUGH))
	{
		ScrollBarParameters scrollbar;

		scrollbar.horizontal = TRUE;
		scrollbar.junction   = 0; //clearlooks_scrollbar_get_junction (widget);

#if 0
		if (CHECK_HINT (GE_HINT_HSCROLLBAR))
			scrollbar.horizontal = TRUE;
		else if (CHECK_HINT (GE_HINT_VSCROLLBAR))
			scrollbar.horizontal = FALSE;
		else /* Fallback based on the size  ... */
#endif
			scrollbar.horizontal = width >= height;

		style_functions->draw_scrollbar_trough (cr, engine, &scrollbar,
							x, y, width, height);
	}
	else if (gtk_theming_engine_has_class (engine, "progressbar"))
	{
		ProgressBarParameters progressbar;
		gdouble               elapsed = 0.0;

#ifdef HAVE_WORKING_ANIMATION
		if(clearlooks_style->animation && clearlooks_animation_is_progressbar (widget))
		{
#warning Assuming non-pulsing progress bars because there is currently no way to query them in GTK+ 3.0.
			clearlooks_animation_progressbar_add ((gpointer)widget);
		}

		elapsed = clearlooks_animation_elapsed (widget);
#endif

#if 0
		if (widget && GE_IS_PROGRESS_BAR (widget))
		{
                        if (gtk_orientable_get_orientation (GTK_ORIENTABLE (widget)))
                        {
                           if (gtk_progress_bar_get_inverted (GTK_PROGRESS_BAR (widget)))
                             progressbar.orientation = CL_ORIENTATION_RIGHT_TO_LEFT;
                           else
                             progressbar.orientation = CL_ORIENTATION_LEFT_TO_RIGHT;
                        }
                        else
                        {
                           if (gtk_progress_bar_get_inverted (GTK_PROGRESS_BAR (widget)))
                             progressbar.orientation = CL_ORIENTATION_BOTTOM_TO_TOP;
                           else
                             progressbar.orientation = CL_ORIENTATION_TOP_TO_BOTTOM;
                        }
			progressbar.value = gtk_progress_bar_get_fraction(GTK_PROGRESS_BAR(widget));
#warning Assuming non-pulsing progress bars because there is currently no way to query them in GTK+ 3.0.
			progressbar.pulsing = FALSE;
		}
		else
#endif
		{
			progressbar.orientation = CL_ORIENTATION_LEFT_TO_RIGHT;
			progressbar.value = 0;
			progressbar.pulsing = FALSE;
		}

		if (direction == GTK_TEXT_DIR_RTL)
		{
			if (progressbar.orientation == CL_ORIENTATION_LEFT_TO_RIGHT)
				progressbar.orientation = CL_ORIENTATION_RIGHT_TO_LEFT;
			else if (progressbar.orientation == CL_ORIENTATION_RIGHT_TO_LEFT)
				progressbar.orientation = CL_ORIENTATION_LEFT_TO_RIGHT;
		}

#if 0
#error No way are we going to reset clips in a proper theme engine.
		/* Following is a hack to have a larger clip area, the one passed in
		 * does not allow for the shadow. */
		if (area)
		{
			GdkRectangle tmp = *area;
			if (!progressbar.pulsing)
			{
				switch (progressbar.orientation)
				{
					case CL_ORIENTATION_RIGHT_TO_LEFT:
						tmp.x -= 1;
					case CL_ORIENTATION_LEFT_TO_RIGHT:
						tmp.width += 1;
						break;
					case CL_ORIENTATION_BOTTOM_TO_TOP:
						tmp.y -= 1;
					case CL_ORIENTATION_TOP_TO_BOTTOM:
						tmp.height += 1;
						break;
				}
			}
			else
			{
				if (progressbar.orientation == CL_ORIENTATION_RIGHT_TO_LEFT ||
				    progressbar.orientation == CL_ORIENTATION_LEFT_TO_RIGHT)
				{
					tmp.x -= 1;
					tmp.width += 2;
				}
				else
				{
					tmp.y -= 1;
					tmp.height += 2;
				}
			}

			cairo_reset_clip (cr);
			gdk_cairo_rectangle (cr, &tmp);
			cairo_clip (cr);
		}
#endif
		style_functions->draw_progressbar_fill (cr, engine, &progressbar,
							x, y, width, height,
							10 - (int)(elapsed * 10.0) % 10);
	}
	else if (gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_ENTRY) &&
		 gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_PROGRESSBAR))
	{
		EntryProgressParameters progress;

		progress.max_size_known = FALSE;
		progress.max_size.x = 0;
		progress.max_size.y = 0;
		progress.max_size.width = 0;
		progress.max_size.height = 0;
#if 0
		progress.border.left = style->xthickness;
		progress.border.right = style->xthickness;
		progress.border.top = style->ythickness;
		progress.border.bottom = style->ythickness;

		if (GE_IS_ENTRY (widget))
		{
			GtkBorder *border;
			/* Try to retrieve the style property. */
			gtk_widget_style_get (widget,
			                      "progress-border", &border,
			                      NULL);

			if (border)
			{
				progress.border = *border;
				gtk_border_free (border);
			}

			/* We got an entry, but well, we may not be drawing to
			 * this particular widget ... it may not even be realized.
			 * Also, we need to be drawing on a window obviously ... */
			if (gtk_widget_get_realized (widget) &&
			    gdk_window_is_visible (gtk_widget_get_window (widget)))
			{
				/* Assumptions done by this code:
				 *  - GtkEntry has some nested windows.
				 *  - gtk_widget_get_window (widget) is the entries window
				 *  - gtk_widget_get_window (widget) is the size of the entry part
				 *    (and not larger)
				 *  - only one layer of subwindows
				 * These should be true with any GTK+ 2.x version.
				 */

                                progress.max_size_known = TRUE;
                                progress.max_size.width = gdk_window_get_width (gtk_widget_get_window (widget));
                                progress.max_size.height = gdk_window_get_height (gtk_widget_get_window (widget));

				/* Now, one more thing needs to be done. If interior-focus
				 * is off, then the entry may be a bit smaller. */
				if (progress.max_size_known && gtk_widget_has_focus (widget))
				{
					gboolean interior_focus = TRUE;
					gint focus_line_width = 1;

					gtk_widget_style_get (widget,
					                      "interior-focus", &interior_focus,
					                      "focus-line-width", &focus_line_width,
					                      NULL);

					if (!interior_focus)
					{
						progress.max_size.x += focus_line_width;
						progress.max_size.y += focus_line_width;
						progress.max_size.width -= 2*focus_line_width;
						progress.max_size.height -= 2*focus_line_width;
					}
				}
				
				if (progress.max_size_known)
				{
					progress.max_size.x += progress.border.left;
					progress.max_size.y += progress.border.top;
					progress.max_size.width -= progress.border.left + progress.border.right;
					progress.max_size.height -= progress.border.top + progress.border.bottom;

					/* Now test that max_size.height == height, if that
					 * fails, something has gone wrong ... so then throw away
					 * the max_size information. */
					if (progress.max_size.height != height)
					{
						progress.max_size_known = FALSE;
						progress.max_size.x = 0;
						progress.max_size.y = 0;
						progress.max_size.width = 0;
						progress.max_size.height = 0;
					}
				}
			}
		}
#endif

		style_functions->draw_entry_progress (cr, engine, &progress,
						      x, y, width, height);
	}
	else if (gtk_theming_engine_has_class (engine, "menuitem"))
	{
		if (gtk_theming_engine_has_class (engine, "menubar"))
		{
#if 0
			params.corners = CR_CORNER_TOPLEFT | CR_CORNER_TOPRIGHT;
#endif
			height += 1;
			style_functions->draw_menubaritem (cr, engine, x, y, width, height);
		}
		else
		{
#if 0
			params.corners = CR_CORNER_ALL;
#endif
			style_functions->draw_menuitem (cr, engine, x, y, width, height);
		}
	}
	else if (gtk_theming_engine_has_class (engine, "scrollbar"))
	{
		ScrollBarParameters scrollbar;
		ScrollBarStepperParameters stepper;
		gboolean colorize_scrollbar;
		CairoColor *scrollbar_color;

		scrollbar.has_color  = FALSE;
		scrollbar.horizontal = TRUE;
		scrollbar.junction   = 0; //clearlooks_scrollbar_get_junction (widget);

		gtk_theming_engine_get (engine, state,
					"-clearlooks-colorize-scrollbar", &colorize_scrollbar,
					"-clearlooks-scrollbar-color", &scrollbar_color,
					NULL);

		if (colorize_scrollbar || scrollbar_color)
			scrollbar.has_color = TRUE;

#if 0
		scrollbar.horizontal = g_str_has_prefix (detail, "hscrollbar");

		if (g_str_equal(detail + 10, "_start"))
			stepper.stepper = CL_STEPPER_START;
		else if (g_str_equal(detail + 10, "_end"))
			stepper.stepper = CL_STEPPER_END;
		else if (g_str_equal(detail + 10, "_start_inner"))
			stepper.stepper = CL_STEPPER_START_INNER;
		else if (g_str_equal(detail + 10, "_end_inner"))
			stepper.stepper = CL_STEPPER_END_INNER;
		else
#endif
			stepper.stepper = CL_STEPPER_UNKNOWN;

		style_functions->draw_scrollbar_stepper (cr, engine, &scrollbar, &stepper,
							 x, y, width, height);

		gdk_rgba_free ((GdkRGBA *) scrollbar_color);
	}
	else if (gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_TOOLBAR) ||
		 gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_DOCK))
	{
		ToolbarParameters toolbar;
		gboolean horizontal;

		//clearlooks_set_toolbar_parameters (&toolbar, widget, x, y);
		toolbar.style = 0; //clearlooks_style->toolbarstyle;

#if 0
		if ((DETAIL ("handlebox_bin") || DETAIL ("dockitem_bin")) && GE_IS_BIN (widget))
		{
			GtkWidget* child = gtk_bin_get_child ((GtkBin*) widget);
			/* This is to draw the correct shadow on the handlebox.
			 * We need to draw it here, as otherwise the handle will not get the
			 * background. */
			if (GE_IS_TOOLBAR (child))
				gtk_widget_style_get (child, "shadow-type", &shadow_type, NULL);
		}
#endif

		horizontal = height < 2*width;
		/* This is not that great. Ideally we would have a nice vertical toolbar. */
		if (horizontal)
			style_functions->draw_toolbar (cr, engine, &toolbar, x, y, width, height);
	}
	else if (gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_MENU))
	{
		style_functions->draw_menu_frame (cr, engine, x, y, width, height);
	}
#if 0
	else if (DETAIL ("hseparator") || DETAIL ("vseparator"))
	{
		gchar *new_detail = (gchar*) detail;
		/* Draw a normal separator, we just use this because it gives more control
		 * over sizing (currently). */

		/* This isn't nice ... but it seems like the best cleanest way to me right now.
		 * It will get slightly nicer in the future hopefully. */
		if (GE_IS_MENU_ITEM (widget))
			new_detail = "menuitem";

		if (DETAIL ("hseparator"))
		{
			gtk_paint_hline (style, cr, state_type, widget, new_detail,
			                       x, x + width - 1, y + height/2);
		}
		else
			gtk_paint_vline (style, cr, state_type, widget, new_detail,
			                       y, y + height - 1, x + width/2);
	}
#endif
	else
	{
		GTK_THEMING_ENGINE_CLASS (clearlooks_engine_parent_class)->render_frame (engine, cr,
											 x, y, width, height);
	}
}

static void
clearlooks_engine_render_slider (GtkThemingEngine *engine,
				 cairo_t          *cr,
				 gdouble           x,
				 gdouble           y,
				 gdouble           width,
				 gdouble           height,
				 GtkOrientation    orientation)
{
	ClearlooksStyleFunctions *style_functions;

	GE_CAIRO_INIT

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);

	if (gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_SLIDER))
	{
		ScrollBarParameters scrollbar;
		gboolean colorize_scrollbar;
		CairoColor *scrollbar_color;
		ClearlooksStyle style;
		GtkStateFlags state;

		scrollbar.has_color  = FALSE;
		scrollbar.horizontal = (orientation == GTK_ORIENTATION_HORIZONTAL);
		scrollbar.junction   = 0; //clearlooks_scrollbar_get_junction (widget);

		state = gtk_theming_engine_get_state (engine);

		gtk_theming_engine_get (engine, state,
					"-clearlooks-colorize-scrollbar", &colorize_scrollbar,
					"-clearlooks-scrollbar-color", &scrollbar_color,
					"-clearlooks-style", &style,
					NULL);

		if (colorize_scrollbar)
		{
			clearlooks_lookup_spot (engine, 1, (GdkRGBA *) &scrollbar.color);
			scrollbar.has_color = TRUE;
		}

		/* Set scrollbar color */
		if (colorize_scrollbar && scrollbar_color)
		{
			scrollbar.color = *scrollbar_color;
			scrollbar.has_color = TRUE;
		}

		if ((style == CLEARLOOKS_STYLE_GLOSSY || style == CLEARLOOKS_STYLE_GUMMY) && !scrollbar.has_color) {
			CairoColor *bg_color;

			gtk_theming_engine_get (engine, state,
						"background-color", &bg_color,
						NULL);

			scrollbar.color = *bg_color;
			gdk_rgba_free ((GdkRGBA *) bg_color);
		}

		style_functions->draw_scrollbar_slider (cr, engine, &scrollbar,
							x, y, width, height);

		gdk_rgba_free ((GdkRGBA *) scrollbar_color);
	}
	else
	{
		SliderParameters slider;

		slider.horizontal = (orientation == GTK_ORIENTATION_HORIZONTAL);
		slider.lower = FALSE;
		slider.fill_level = FALSE;

		style_functions->draw_slider_button (cr, engine, &slider,
						     x, y, width, height);
	}
}

static void
clearlooks_engine_render_option (GtkThemingEngine *engine,
				 cairo_t          *cr,
				 gdouble           x,
				 gdouble           y,
				 gdouble           width,
				 gdouble           height)
{
	ClearlooksStyleFunctions *style_functions;
	CheckboxParameters checkbox;

	GE_CAIRO_INIT

	checkbox.in_menu = gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_MENU);

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);

	style_functions->draw_radiobutton (cr, engine, &checkbox, x, y, width, height);
}

static void
clearlooks_engine_render_check (GtkThemingEngine *engine,
				cairo_t          *cr,
				gdouble           x,
				gdouble           y,
				gdouble           width,
				gdouble           height)
{
	ClearlooksStyleFunctions *style_functions;
	CheckboxParameters checkbox;

	GE_CAIRO_INIT

	checkbox.in_cell = gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_CELL);
	checkbox.in_menu = gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_MENU);

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);
	style_functions->draw_checkbox (cr, engine, &checkbox,
					x, y, width, height);
}

#if 0
static void
clearlooks_style_draw_vline (GtkStyle               *style,
                             cairo_t                *cr,
                             GtkStateType            state_type,
                             GtkWidget              *widget,
                             const gchar            *detail,
                             gint                    y1,
                             gint                    y2,
                             gint                    x)
{
	ClearlooksStyle *clearlooks_style = CLEARLOOKS_STYLE (style);
	const ClearlooksColors *colors;
	SeparatorParameters separator = { FALSE };

	CHECK_ARGS

	colors = &clearlooks_style->colors;

	/* There is no such thing as a vertical menu separator
	 * (and even if, a normal one should be better on menu bars) */
	STYLE_FUNCTION(draw_separator) (cr, colors, NULL, &separator,
	                                x, y1, 2, y2-y1+1);
}

static void
clearlooks_style_draw_hline (GtkStyle               *style,
                             cairo_t                *cr,
                             GtkStateType            state_type,
                             GtkWidget              *widget,
                             const gchar            *detail,
                             gint                    x1,
                             gint                    x2,
                             gint                    y)
{
	ClearlooksStyle *clearlooks_style = CLEARLOOKS_STYLE (style);
	const ClearlooksColors *colors;
	SeparatorParameters separator;

	CHECK_ARGS

	colors = &clearlooks_style->colors;

	separator.horizontal = TRUE;

	if (!DETAIL ("menuitem"))
		STYLE_FUNCTION(draw_separator) (cr, colors, NULL, &separator,
		                                x1, y, x2-x1+1, 2);
	else
		STYLE_FUNCTION(draw_menu_item_separator) (cr, colors, NULL, &separator,
		                                          x1, y, x2-x1+1, 2);
}
#endif

static void
clearlooks_engine_render_arrow (GtkThemingEngine *engine,
				cairo_t          *cr,
				gdouble           angle,
				gdouble           x,
				gdouble           y,
				gdouble           size)
{
	ClearlooksStyleFunctions *style_functions;
	ArrowParameters  arrow;

	GE_CAIRO_INIT;

	arrow.type = CL_ARROW_NORMAL;
	arrow.angle = angle;

	if (gtk_theming_engine_has_class (engine, "combo") &&
	    !gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_ENTRY))
	{
		arrow.type = CL_ARROW_COMBO;
	}

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);
	style_functions->draw_arrow (cr, engine, &arrow, x, y, size, size);
}

static void
clearlooks_engine_render_focus (GtkThemingEngine *engine,
				cairo_t          *cr,
				gdouble           x,
				gdouble           y,
				gdouble           width,
				gdouble           height)
{
	ClearlooksStyleFunctions *style_functions;
	CairoColor *focus_color, *focus_fill_color, *bg_color;
	const GtkWidgetPath *path;
	FocusParameters focus;
	GtkStateFlags state;
	gboolean disable_focus;
	guint8* dash_list;

	GE_CAIRO_INIT

	state = gtk_theming_engine_get_state (engine);
	gtk_theming_engine_get (engine, state,
				"-clearlooks-disable-focus", &disable_focus,
				NULL);

	clearlooks_lookup_functions (CLEARLOOKS_ENGINE (engine),
				     &style_functions, NULL);

	path = gtk_theming_engine_get_path (engine);

	/* Just return if focus drawing is disabled. */
	if (disable_focus)
		return;

	gtk_theming_engine_get (engine, state,
				"-clearlooks-focus-color", &focus_color,
				"-clearlooks-focus-fill-color", &focus_fill_color,
				NULL);

	gtk_theming_engine_get (engine, GTK_STATE_FLAG_SELECTED,
				"background-color", &bg_color,
				NULL);

#if 0
	/* Corners */
	params.corners = CR_CORNER_ALL;
	if (CHECK_HINT (GE_HINT_COMBOBOX_ENTRY))
	{
		if (params.ltr)
			params.corners = CR_CORNER_TOPRIGHT | CR_CORNER_BOTTOMRIGHT;
		else
			params.corners = CR_CORNER_TOPLEFT | CR_CORNER_BOTTOMLEFT;

		if (params.xthickness > 2)
		{
			if (params.ltr)
				x--;
			width++;
		}
	}
#endif

	focus.has_color = FALSE;
	focus.interior = FALSE;
	focus.line_width = 1;
	focus.padding = 1;
	dash_list = NULL;

	gtk_theming_engine_get_style (engine,
		                      "focus-line-width", &focus.line_width,
		                      "focus-line-pattern", &dash_list,
		                      "focus-padding", &focus.padding,
		                      "interior-focus", &focus.interior,
				      NULL);

	if (dash_list)
		focus.dash_list = dash_list;
	else
		focus.dash_list = (guint8*) g_strdup ("\1\1");

	/* Focus type */
	if (gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_BUTTON))
	{
#if 0
		if (CHECK_HINT (GE_HINT_TREEVIEW_HEADER))
		{
			focus.type = CL_FOCUS_TREEVIEW_HEADER;
		}
		else
#endif
		{
			GtkReliefStyle relief = GTK_RELIEF_NORMAL;
#if 0
			/* Check for the shadow type. */
			if (widget && GTK_IS_BUTTON (widget))
				g_object_get (G_OBJECT (widget), "relief", &relief, NULL);
#endif

			if (relief == GTK_RELIEF_NORMAL)
				focus.type = CL_FOCUS_BUTTON;
			else
				focus.type = CL_FOCUS_BUTTON_FLAT;

#if 0
			/* This is a workaround for the bogus focus handling that
			 * clearlooks has currently.
			 * I truely dislike putting it here, but I guess it is better
			 * then having such a visible bug. It should be removed in the
			 * next unstable release cycle.  -- Benjamin */
			if (ge_object_is_a (G_OBJECT (widget), "ButtonWidget"))
				focus.type = CL_FOCUS_LABEL;
#endif
		}
	}
#if 0
	else if (detail && g_str_has_prefix (detail, "treeview"))
	{
		/* Focus in a treeview, and that means a lot of different detail strings. */
		if (g_str_has_prefix (detail, "treeview-drop-indicator"))
			focus.type = CL_FOCUS_TREEVIEW_DND;
		else
			focus.type = CL_FOCUS_TREEVIEW_ROW;

		if (g_str_has_suffix (detail, "left"))
		{
			focus.continue_side = CL_CONT_RIGHT;
		}
		else if (g_str_has_suffix (detail, "right"))
		{
			focus.continue_side = CL_CONT_LEFT;
		}
		else if (g_str_has_suffix (detail, "middle"))
		{
			focus.continue_side = CL_CONT_LEFT | CL_CONT_RIGHT;
		}
		else
		{
			focus.continue_side = CL_CONT_NONE;
		}

	}
#endif
	else if (gtk_widget_path_is_type (path, GTK_TYPE_SCALE) &&
		 gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_TROUGH))
	{
		focus.type = CL_FOCUS_SCALE;
	}
	else if (gtk_theming_engine_has_region (engine, GTK_STYLE_REGION_TAB, NULL))
	{
		focus.type = CL_FOCUS_TAB;
	}
#if 0
	else if (detail && g_str_has_prefix (detail, "colorwheel"))
	{
		if (DETAIL ("colorwheel_dark"))
			focus.type = CL_FOCUS_COLOR_WHEEL_DARK;
		else
			focus.type = CL_FOCUS_COLOR_WHEEL_LIGHT;
	}
	else if (DETAIL("checkbutton") || DETAIL("radiobutton") || DETAIL("expander"))
	{
		focus.type = CL_FOCUS_LABEL; /* Let's call it "LABEL" :) */
	}
	else if (CHECK_HINT (GE_HINT_TREEVIEW))
	{
		focus.type = CL_FOCUS_TREEVIEW; /* Treeview without content is focused. */
	}
	else if (DETAIL("icon_view"))
	{
		focus.type = CL_FOCUS_ICONVIEW;
	}
#endif
	else
	{
		focus.type = CL_FOCUS_UNKNOWN; /* Custom widgets (Beagle) and something unknown */
	}

	/* Focus color */
	if (focus_color)
	{
		focus.color = *focus_color;
		focus.has_color = TRUE;
	}
	else
		focus.color = *bg_color;

	if (focus_fill_color)
		focus.fill_color = *focus_fill_color;
	else
	{
		focus.fill_color = *bg_color;
		focus.fill_color.a = 0.05;
	}


	style_functions->draw_focus (cr, engine, &focus, x, y, width, height);

	g_free (focus.dash_list);
	gdk_rgba_free ((GdkRGBA *) focus_color);
	gdk_rgba_free ((GdkRGBA *) focus_fill_color);
	gdk_rgba_free ((GdkRGBA *) bg_color);
}

static GdkPixbuf *
set_transparency (const GdkPixbuf *pixbuf, gdouble alpha_percent)
{
	GdkPixbuf *target;
	guchar *data, *current;
	guint x, y, rowstride, height, width;

	g_return_val_if_fail (pixbuf != NULL, NULL);
	g_return_val_if_fail (GDK_IS_PIXBUF (pixbuf), NULL);

	/* Returns a copy of pixbuf with it's non-completely-transparent pixels to
	   have an alpha level "alpha_percent" of their original value. */

	target = gdk_pixbuf_add_alpha (pixbuf, FALSE, 0, 0, 0);

	if (alpha_percent == 1.0)
		return target;
	width = gdk_pixbuf_get_width (target);
	height = gdk_pixbuf_get_height (target);
	rowstride = gdk_pixbuf_get_rowstride (target);
	data = gdk_pixbuf_get_pixels (target);

	for (y = 0; y < height; y++)
	{
		for (x = 0; x < width; x++)
		{
			/* The "4" is the number of chars per pixel, in this case, RGBA,
			   the 3 means "skip to the alpha" */
			current = data + (y * rowstride) + (x * 4) + 3;
			*(current) = (guchar) (*(current) * alpha_percent);
		}
	}

	return target;
}

static GdkPixbuf*
scale_or_ref (GdkPixbuf *src,
              int width,
              int height)
{
	if (width == gdk_pixbuf_get_width (src) &&
	    height == gdk_pixbuf_get_height (src))
	{
		return g_object_ref (src);
	}
	else
	{
		return gdk_pixbuf_scale_simple (src,
		                                width, height,
		                                GDK_INTERP_BILINEAR);
	}
}

static void
clearlooks_engine_render_layout (GtkThemingEngine *engine,
				 cairo_t          *cr,
				 gdouble           x,
				 gdouble           y,
				 PangoLayout      *layout)
{
	GtkStateFlags state;
	GdkRGBA *color;

	GE_CAIRO_INIT;

        ge_cairo_transform_for_layout (cr, layout, x, y);

	state = gtk_theming_engine_get_state (engine);
	gtk_theming_engine_get (engine, state,
				"color", &color,
				NULL);

	gdk_cairo_set_source_rgba (cr, color);
        gdk_rgba_free (color);

	if (state == 0 &&
	    gtk_theming_engine_has_class (engine, GTK_STYLE_CLASS_ACCELERATOR)) {
		CairoColor *bg_color, *color, mix;
		gdouble accel_label_shade;

		gtk_theming_engine_get (engine, state,
					"background-color", &bg_color,
					"-clearlooks-accel-label-shade", &accel_label_shade,
					"color", &color,
					NULL);

		ge_mix_color (bg_color, color,
			      accel_label_shade,
			      &mix);

		ge_cairo_set_color (cr, &mix);

		gdk_rgba_free ((GdkRGBA *) bg_color);
		gdk_rgba_free ((GdkRGBA *) color);
	}

	if (state & GTK_STATE_FLAG_INSENSITIVE)
	{
		CairoColor *color, temp;

		gtk_theming_engine_get (engine, state,
					"background-color", &color,
					NULL);

		ge_shade_color (color, 1.2, &temp);

                cairo_save (cr);

		ge_cairo_set_color (cr, &temp);
                cairo_move_to (cr, 1, 1);
                pango_cairo_show_layout (cr, layout);

                cairo_restore (cr);

		gdk_rgba_free ((GdkRGBA *) color);
	}

        pango_cairo_show_layout (cr, layout);
}

static GdkPixbuf *
clearlooks_engine_render_icon_pixbuf (GtkThemingEngine    *engine,
				      const GtkIconSource *source,
				      GtkIconSize          size)
{
	int width = 1;
	int height = 1;
	GdkPixbuf *scaled;
	GdkPixbuf *stated;
	GdkPixbuf *base_pixbuf;
	GdkScreen *screen;
	GtkSettings *settings;
	GtkStateFlags state;

	base_pixbuf = gtk_icon_source_get_pixbuf (source);
	screen = gtk_theming_engine_get_screen (engine);
	settings = gtk_settings_get_for_screen (screen);
	state = gtk_theming_engine_get_state (engine);

	g_return_val_if_fail (base_pixbuf != NULL, NULL);

	if (size != (GtkIconSize) -1 && !gtk_icon_size_lookup_for_settings (settings, size, &width, &height))
	{
		g_warning (G_STRLOC ": invalid icon size '%d'", size);
		return NULL;
	}

	/* If the size was wildcarded, and we're allowed to scale, then scale; otherwise,
	 * leave it alone.
	 */
	if (size != (GtkIconSize)-1 && gtk_icon_source_get_size_wildcarded (source))
		scaled = scale_or_ref (base_pixbuf, width, height);
	else
		scaled = g_object_ref (base_pixbuf);

	/* If the state was wildcarded, then generate a state. */
	if (gtk_icon_source_get_state_wildcarded (source))
	{
		if (state & GTK_STATE_FLAG_INSENSITIVE)
		{
			stated = set_transparency (scaled, 0.3);
			gdk_pixbuf_saturate_and_pixelate (stated, stated, 0.1, FALSE);

			g_object_unref (scaled);
		}
		else if (state & GTK_STATE_FLAG_PRELIGHT)
		{
			stated = gdk_pixbuf_copy (scaled);

			gdk_pixbuf_saturate_and_pixelate (scaled, stated, 1.2, FALSE);

			g_object_unref (scaled);
		}
		else
		{
			stated = scaled;
		}
	}
	else
		stated = scaled;

	return stated;
}

void
clearlooks_engine_register_types (GTypeModule *module)
{
  clearlooks_engine_register_type (module);
}

static void
clearlooks_engine_init (ClearlooksEngine *clearlooks_engine)
{
	clearlooks_register_style_classic (&clearlooks_engine->style_functions[CLEARLOOKS_STYLE_CLASSIC],
					   &clearlooks_engine->style_constants[CLEARLOOKS_STYLE_CLASSIC]);

	clearlooks_engine->style_functions[CLEARLOOKS_STYLE_GNOME3] =
                clearlooks_engine->style_functions[CLEARLOOKS_STYLE_CLASSIC];
	clearlooks_engine->style_constants[CLEARLOOKS_STYLE_GNOME3] =
                clearlooks_engine->style_constants[CLEARLOOKS_STYLE_CLASSIC];

	clearlooks_register_style_gnome3 (&clearlooks_engine->style_functions[CLEARLOOKS_STYLE_GNOME3],
                                          &clearlooks_engine->style_constants[CLEARLOOKS_STYLE_GNOME3]);

	/* FIXME: Register other skins as they get ported to GtkThemingEngine */
}

static void
clearlooks_engine_class_init (ClearlooksEngineClass * klass)
{
	GtkThemingEngineClass *engine_class = GTK_THEMING_ENGINE_CLASS (klass);

	engine_class->render_arrow = clearlooks_engine_render_arrow;
	engine_class->render_check = clearlooks_engine_render_check;
	engine_class->render_option = clearlooks_engine_render_option;
	engine_class->render_slider = clearlooks_engine_render_slider;
	engine_class->render_extension = clearlooks_engine_render_extension;
	/*
	engine_class->render_background = clearlooks_engine_render_background;
	*/
	engine_class->render_frame = clearlooks_engine_render_frame;
	engine_class->render_frame_gap = clearlooks_engine_render_frame_gap;
	engine_class->render_handle = clearlooks_engine_render_handle;
	engine_class->render_layout = clearlooks_engine_render_layout;
	engine_class->render_focus = clearlooks_engine_render_focus;
	engine_class->render_icon_pixbuf = clearlooks_engine_render_icon_pixbuf;

        gtk_theming_engine_register_property (CLEARLOOKS_NAMESPACE, NULL,
                                              g_param_spec_boolean ("disable-focus",
                                                                    "Disable focus",
                                                                    "Disable focus",
                                                                    FALSE, 0));
	gtk_theming_engine_register_property (CLEARLOOKS_NAMESPACE, NULL,
                                              g_param_spec_boolean ("colorize-scrollbar",
                                                                    "Colorize scrollbar",
                                                                    "Colorize scrollbar",
                                                                    FALSE, 0));
	gtk_theming_engine_register_property (CLEARLOOKS_NAMESPACE, NULL,
                                              g_param_spec_boxed ("focus-color",
                                                                  "Focus color",
                                                                  "Focus color",
                                                                  GDK_TYPE_RGBA, 0));
	gtk_theming_engine_register_property (CLEARLOOKS_NAMESPACE, NULL,
                                              g_param_spec_boxed ("focus-fill-color",
                                                                  "Focus fill color",
                                                                  "Focus fill color",
                                                                  GDK_TYPE_RGBA, 0));
	gtk_theming_engine_register_property (CLEARLOOKS_NAMESPACE, NULL,
                                              g_param_spec_boxed ("scrollbar-color",
                                                                  "Scrollbar color",
                                                                  "Scrollbar color",
                                                                  GDK_TYPE_RGBA, 0));
	gtk_theming_engine_register_property (CLEARLOOKS_NAMESPACE, NULL,
                                              g_param_spec_double ("contrast",
                                                                   "Contrast",
                                                                   "Contrast",
                                                                   0, 10, 1, 0));
	gtk_theming_engine_register_property (CLEARLOOKS_NAMESPACE, NULL,
                                              g_param_spec_double ("shade",
                                                                   "Shade",
                                                                   "Shade",
                                                                   0, 10, 0, 0));
	gtk_theming_engine_register_property (CLEARLOOKS_NAMESPACE, NULL,
                                              g_param_spec_double ("accel-label-shade",
                                                                   "Accelerator label shade",
                                                                   "Accelerator label shade",
                                                                   0, 10, 0, 0));
	gtk_theming_engine_register_property (CLEARLOOKS_NAMESPACE, NULL,
                                              g_param_spec_enum ("style",
                                                                 "Clearlooks Style",
                                                                 "Clearlooks Style",
                                                                 CLEARLOOKS_TYPE_STYLE,
                                                                 CLEARLOOKS_STYLE_CLASSIC,
                                                                 0));
}

static void
clearlooks_engine_class_finalize (ClearlooksEngineClass *klass)
{
}
